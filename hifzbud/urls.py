"""hifzbud URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls.conf import include, path
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic.base import RedirectView

from secret_word.views import SecretWordChallangeView, SecretWordChallangeAPIView, SecretWordChallangeListAPIView

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    url('track/', include('hifztrack.urls')),
    url('simple/', include('simpletrack.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/track', permanent=False)),
    path('challanges/current', SecretWordChallangeView.as_view(), name='curr_challenge'),
    path('challenges/<int:pk>', SecretWordChallangeAPIView.as_view(), name='challenge-update'),
    path('challenges', SecretWordChallangeListAPIView.as_view(), name='challenge-list'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

