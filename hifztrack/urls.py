''' 
Created on 5 avr. 2018

@author: omar
'''
from django.urls.conf import path, include
from django.conf.urls import url
from hifztrack.views import ui, api
from datetime import date
from django.shortcuts import redirect
from django.http.response import HttpResponse

from hifztrack.views.rest import HalkaListCreateAPIView, HalkaDetailAPIView, CSVUserUpload

template_name ="talib/halkamonth.html"

urlpatterns = [path('', ui.homepage, name='index'),
               path('cheikh/home', ui.cheikh_home, name='cheikh_home'),
               path('halkas/list', ui.CheikhHomePerHalka.as_view(), name='halka_list'),
               path('talibs/list', ui.CheikhHomePerTalib.as_view(), name='talib_list'),
               path('mywall', lambda r: redirect('mywall',year=date.today().year),name='mywall_current'),
               path('mywall/<int:year>', ui.AwardWallView.as_view(),name='mywall'),
               path('myhalkas/<int:pk>/dashboard', ui.HalkaDashboardView.as_view(),name='dashboard'),
               path('myhalkas/dashboard', ui.dashboard,name='any_dashboard'),
               path('halkas/<int:pk>/talib/<str:talib>/dashboard', ui.HalkaDashboardView.as_view(cheikh_mode=True),name='cheikh_dashboard'),
               path('halkas/<int:pk>/talib/<str:talib>/<int:year>/<int:month>/overview', ui.HalkaMonthOverview.as_view(), name='month_overview'),
               path('halkas/<int:pk>/talib/<str:talib>/current/overview', ui.HalkaCurrentMonthOverview.as_view(), name='curr_month_overview'),
               path('halkas/<int:pk>/talib/<str:talib>/progress', ui.HalkaProgressList.as_view(), name='progress'),
               url('qurandata/', include('qurandata.urls')),
               path('goalnote/<int:pk>/exercise', ui.GoalNoteExercise.as_view(),name='exercise'),
               path('sohba', ui.sohba, name='any_sohba'),
               path('sohba/<int:pk>/view', ui.SohbaView.as_view(),name='sohba'),
               path('myhalkas/<int:pk>/selfreview', ui.SelfReviewView.as_view(),name='selfreview'),
               path('talib/<str:talib>/tracking.xlsx', ui.TrackingXLSXSheet.as_view(), name='tracking_xlsx'),
               path('mytracking.xlsx',ui.TrackingXLSXSheet.as_view(), name='mytracking_xlsx'),
               path('api', lambda r: HttpResponse("404",status=404), name='api_root'),
               path('api/talib/<str:talibuser>/goal/<int:goalpk>/notes',ui.api_get_notes_by_goal,name='api_get_notes_by_goal'),
               path('api/halka/<int:halkapk>/talib/<str:talibuser>/notes',ui.api_get_notes_by_talib,name='api_get_notes_by_talib'),
               path('api/goal/<int:goalpk>/ayaselector',ui.api_get_ayasel_by_goal,name='api_get_ayasel_by_goal'),
               path('api/rangesync',api.api_sync_ayaranges,name='api_rangesync'),
               path('api/halka/', HalkaListCreateAPIView.as_view(), name='halka-list-create'),
               path('api/halka/<int:pk>/', HalkaDetailAPIView.as_view(), name='halka-detail'),
               path('api/talibs', CSVUserUpload.as_view(), name='talibs-csv')
            ]