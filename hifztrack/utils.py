'''
Created on 29 avr. 2018

@author: omar
'''
import calendar
from datetime import date
import re
import qurandata
from django.utils.safestring import mark_safe


def last_day_of_month(year, month):
    return date(year, month, calendar.monthrange(year, month)[1])

def annotate_quranrefs(text, context=None):
    for ref in qurandata.refs_from_text(text, context):
        text = text.replace(ref.expr,"""
            <span class="quranref" data-sura="%(suraidx)d" data-aya="%(startaya)d" data-ayacount="%(ayacount)d" title="%(desc)s">
                %(expr)s
            </span>""" %  {
                'suraidx':ref.suraidx or 0, 
                'startaya': ref.startaya,
                'ayacount': ref.count(),
                'expr': ref.expr,
                'desc': ref.astext()
            })
    
    return mark_safe(text)

def quranrefs2html(text, context=None):
    replaced=set()
    for ref in qurandata.refs_from_text(text, context):
        if ref.expr in replaced: continue
        replaced.add(ref.expr)
        text = text.replace(ref.expr, ref.ashtml())
    return mark_safe(text)

def quranrefs2str(text, context=None):
    replaced=set()
    for ref in qurandata.refs_from_text(text, context):
        if ref.expr in replaced: continue
        replaced.add(ref.expr)
        text = text.replace(ref.expr, ref.astext())
        
    return mark_safe(text)