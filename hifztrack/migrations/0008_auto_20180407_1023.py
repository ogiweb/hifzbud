# Generated by Django 2.0.4 on 2018-04-07 09:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('django_gamification', '0013_auto_20180324_0605'),
        ('hifztrack', '0007_auto_20180406_2117'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='talibday',
            name='talib',
        ),
        migrations.RemoveField(
            model_name='plannedgoal',
            name='acheived',
        ),
        migrations.RemoveField(
            model_name='plannedgoal',
            name='notified',
        ),
        migrations.RemoveField(
            model_name='talib',
            name='rewards_unlocked',
        ),
        migrations.RemoveField(
            model_name='talib',
            name='total_gold',
        ),
        migrations.RemoveField(
            model_name='talib',
            name='total_points',
        ),
        migrations.AddField(
            model_name='goal',
            name='points_value',
            field=models.IntegerField(default=10, help_text='Represent the importance of goal. Default is 10'),
        ),
        migrations.AddField(
            model_name='plannedgoal',
            name='acheived_time',
            field=models.DateTimeField(help_text='Date Time at which goal was actually acheived, NULL if not yet acheived', null=True),
        ),
        migrations.AddField(
            model_name='plannedgoal',
            name='talib',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='hifztrack.Talib'),
        ),
        migrations.AddField(
            model_name='talib',
            name='game',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='django_gamification.GamificationInterface'),
        ),
        migrations.AddField(
            model_name='talib',
            name='last_dashboard_view_time',
            field=models.DateTimeField(help_text='Last time dashboard was viewed to user successfullay and any progress / awards properly notified. NULL if never opened dashboard', null=True),
        ),
        migrations.AlterField(
            model_name='plannedgoal',
            name='day',
            field=models.DateField(help_text='The day at which this goal is planned to be acheived.'),
        ),
        migrations.DeleteModel(
            name='TalibDay',
        ),
    ]
