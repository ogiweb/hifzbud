# Generated by Django 2.0.4 on 2018-05-01 20:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hifztrack', '0014_plannedgoal_is_bonus'),
    ]

    operations = [
        migrations.CreateModel(
            name='AwardDefinition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('icon_html', models.TextField(verbose_name='HTML of award icon')),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='HalkaAwardOnMonthlyProgressRule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unlock_percent', models.FloatField(default=100.0, verbose_name='Percentage of monthly target to give award.')),
                ('definition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='hifztrack.AwardDefinition')),
                ('halka', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='month_award_rules', to='hifztrack.Halka')),
            ],
        ),
        migrations.RemoveField(
            model_name='level',
            name='definition',
        ),
        migrations.RemoveField(
            model_name='level',
            name='halka',
        ),
        migrations.DeleteModel(
            name='Level',
        ),
        migrations.DeleteModel(
            name='LevelDefinition',
        ),
    ]
