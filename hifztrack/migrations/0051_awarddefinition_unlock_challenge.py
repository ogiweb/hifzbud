# Generated by Django 3.2.18 on 2023-03-19 11:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hifztrack', '0050_auto_20230301_0953'),
    ]

    operations = [
        migrations.AddField(
            model_name='awarddefinition',
            name='unlock_challenge',
            field=models.BooleanField(default=False, verbose_name='Unlock a challenge when on'),
        ),
    ]
