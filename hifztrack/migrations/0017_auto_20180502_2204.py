# Generated by Django 2.0.4 on 2018-05-02 21:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hifztrack', '0016_wonaward'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='goal',
            options={'ordering': ['pk']},
        ),
        migrations.AlterModelOptions(
            name='plannedgoal',
            options={'ordering': ['day', 'pk']},
        ),
        migrations.AlterModelOptions(
            name='wonaward',
            options={'ordering': ['win_time']},
        ),
        migrations.AddField(
            model_name='plannedgoal',
            name='cheikh_note',
            field=models.TextField(default='', verbose_name='Not for Cheikh'),
        ),
        migrations.AddField(
            model_name='plannedgoal',
            name='note',
            field=models.TextField(default='', verbose_name='Note for Talib'),
        ),
    ]
