# Generated by Django 2.0.4 on 2018-09-23 13:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hifztrack', '0032_selfreview'),
    ]

    operations = [
        migrations.AddField(
            model_name='talib',
            name='allow_self_review',
            field=models.BooleanField(default=False, verbose_name='Allow self review'),
        ),
    ]
