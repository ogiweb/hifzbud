from django.test import TestCase
from .models import *
from datetime import date

# Create your tests here.

class FullProcessTest(TestCase):
    def setUp(self):
        self.halka = Halka.objects.create(cheikh=User.objects.create_user("cheickh"), 
                                          name="My Halka", description="Test halka")
        self.goal1 = Goal.objects.create(halka=self.halka, name="Goal1", description="Test goal 1", 
                                         points_value=10)
        self.goal2 = Goal.objects.create(halka=self.halka, name="Goal2", description="Test goal 2", 
                                         points_value=20)
        self.talib1 = Talib.objects.create(halka=self.halka, user=User.objects.create_user("talib1"), points_month_tgt=15)
        self.talib2 = Talib.objects.create(halka=self.halka, user=User.objects.create_user("talib2"))
    
        self.awarddef = AwardDefinition.objects.create(name="testaward1", icon_html="Bravo !")
        HalkaAwardOnMonthlyProgressRule.objects.create(halka=self.halka, definition=self.awarddef, unlock_percent=1)
        HalkaAwardOnMonthlyProgressRule.objects.create(halka=self.halka, definition=self.awarddef, unlock_percent=100)
        
    def test_full_process(self):
        #plan a talibday
        day = datetime.date.today()
        pgoal1 = self.talib1.plan_goal(self.goal1, day)
        pgoal2 = self.talib1.plan_goal(self.goal2, day)
        
        tday = date(2017,1,1)
        tpgoal2 = self.talib1.plan_goal(self.goal2, tday)
        
        #Plan talib1 some goals to check for eventual interfeance.
        self.talib2.plan_goal(self.goal2, day)
        self.talib2.plan_goal(self.goal1, tday)
        
                
        def assertTalibPoints(talib, total, day_target, day_acquired, tday_target, tday_acquired):
            talib.refresh_from_db()
            self.assertEqual(talib.total_points, total)
            
        
        assertTalibPoints(self.talib1, 0, 30, 0, 20, 0)
        assertTalibPoints(self.talib2, 0, 20, 0, 10, 0)
        #assertTrue(self.talib1.strength(day) = 100)
        
        #Cheickh performs that on the goal
        
        self.assertFalse(pgoal1.acheived, None)
        
        pgoal1.mark_acheived(quality=3)
        
        self.assertTrue(pgoal1.acheived)
        
        self.talib1.reward_pending_points([pgoal1.goal])
        assertTalibPoints(self.talib1, 10, 30, 10, 20, 0)
        assertTalibPoints(self.talib2, 0, 20, 0, 10, 0)
        
        
        
        pgoal2.mark_acheived(quality=5)
        tpgoal2.mark_acheived(quality=0)
        
        self.talib1.reward_pending_points([pgoal2.goal])
        assertTalibPoints(self.talib1, 50, 30, 30, 20, 20)
        assertTalibPoints(self.talib2, 0, 20, 0, 10, 0)
        
        
    def test_win_award_pct_rule(self):
        pg=PlannedGoal.objects.create(day=datetime.date(2020,1,3), 
                                      talib=self.talib1, 
                                      goal=self.goal1)
        pg.mark_acheived(4)
        self.talib1.reward_pending_points([self.goal1])
        self.assertEquals(self.talib1.wonaward_set.count(),1)
        self.assertEquals(self.talib1.wonaward_set.get().win_goal, self.goal1)
    
    def test_win_award_goal_tgt(self):
        pg=PlannedGoal.objects.create(day=datetime.date(2020,1,3), 
                                      talib=self.talib1, 
                                      goal=self.goal1)
        #Attach an award to goal
        self.goal1.goal_award=self.awarddef
        self.goal1.save()
        
        pg.mark_acheived(4)
        self.talib1.reward_pending_points([self.goal1])
        self.assertEquals(self.talib1.wonaward_set.count(),2)
        self.assertEquals(set(a.win_goal for a in self.talib1.wonaward_set.all()), {self.goal1})
    
        
    def test_query_for_wonawards(self):
        pg=PlannedGoal.objects.create(day=datetime.date(2020,1,3), 
                                      talib=self.talib1, 
                                      goal=self.goal1)
        pg2=PlannedGoal.objects.create(day=datetime.date(2020,1,5), 
                                      talib=self.talib1, 
                                      goal=self.goal2)
        pg.mark_acheived(4)
        pg2.mark_acheived(4)
        
        self.talib1.reward_pending_points([self.goal1])
        self.assertEquals(self.talib1.get_active_won_awards().count(),1)
        (aw2,),_pts = self.talib1.reward_pending_points([self.goal2])
        self.assertEquals(self.talib1.get_active_won_awards().count(),2)
        #plan a goal for tomorrow
        PlannedGoal.objects.create(day=date.today()+timedelta(days=1), 
                                      talib=self.talib1, 
                                      goal=self.goal1)
        
        self.assertEquals(self.talib1.get_active_won_awards().count(),2, "A PlannedGoal in the future should not have impact")
        WonAward.objects.filter(talib=self.talib1, win_goal=self.goal2)\
            .update(win_time=datetime.datetime(2020,1,2,15,0))
        
        self.assertEquals(self.talib1.get_active_won_awards().count(),1, "Award older than last pg should not be sent")
        
    