from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Halka, Goal, AwardDefinition

class GoalSerializer(serializers.ModelSerializer):
    goal_award = serializers.CharField(write_only=True, allow_blank=True, required=False)

    class Meta:
        model = Goal
        fields = ['name', 'description', 'points_value', 'position', 'start_day', 'end_day', 'goal_award', 'icon_pack']

class HalkaSerializer(serializers.ModelSerializer):
    cheikh = serializers.CharField(write_only=True, allow_blank=True, required=False)
    associate_cheikhs = serializers.ListField(child=serializers.CharField(), write_only=True, required=False)

    goals = GoalSerializer(many=True, allow_null=True, required=False, write_only=True)

    def create(self, validated_data):
        goals_data = validated_data.pop('goals', [])
        cheikh = validated_data.pop('cheikh', None)
        associate_cheikhs = validated_data.pop('associate_cheikhs', [])
        validated_data['cheikh']=User.objects.get(username=cheikh)

        halka = Halka.objects.create(**validated_data)

        for username in associate_cheikhs:
            user = User.objects.get(username=username)
            halka.associate_cheikhs.add(user)

        for i,goal_data in enumerate(goals_data):
            aw = goal_data.pop('goal_award', None)
            if aw is not None:
                aw = AwardDefinition.objects.get(name=aw)
            Goal.objects.create(halka=halka, goal_award=aw, position=i, **goal_data)

        return halka

    def update(self, instance, validated_data):
        raise Exception("Not supported")
        cheikh = validated_data.pop('cheikh', None)
        associate_cheikhs = validated_data.pop('associate_cheikhs', [])

        if cheikh:
            instance.cheikh = User.objects.get(username=cheikh)

        instance.associate_cheikhs.clear()
        for username in associate_cheikhs:
            user = User.objects.get(username=username)
            instance.associate_cheikhs.add(user)

        # Update the rest of the fields
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        return instance

    class Meta:
        model = Halka
        fields = ['cheikh', 'associate_cheikhs', 'name', 'description', 'show_next_award', 'read_access_token', 'write_access_token', 'miss_delay_days', 'goals']
