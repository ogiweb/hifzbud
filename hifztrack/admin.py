from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from import_export.admin import ImportExportModelAdmin

from .models import *


class TalibAdminInline(admin.TabularInline):
    model = Talib
    exclude=('game','last_tgt_upd_suggest')
    readonly_fields=('last_dashboard_view_day',)


class PendingRewardPointsInline(admin.TabularInline):
    model=PendingRewardPoints


class MonthlyAwardRulesInlineAdmin(admin.TabularInline):
    model = HalkaAwardOnMonthlyProgressRule


class GoalInline(admin.TabularInline):
    model = Goal
    fields = ('name', 'points_value', 'goal_award', 'start_day', 'end_day', 'edit_link')
    readonly_fields = ('name', 'icon_pack', 'points_value', 'goal_award', 'start_day', 'end_day', 'edit_link')
    extra = 0
    can_delete = False
    max_num = 0

    @mark_safe
    def edit_link(self, instance):
        url = reverse('admin:hifztrack_goal_change', args=[instance.pk])
        return f'<a href="{url}">Edit</a>'
    edit_link.short_description = 'Edit link'


class HalkaAdmin(ImportExportModelAdmin):
    list_display=('name', 'description', 'cheikh')
    search_fields=('name', 'description', 'cheikh__username','associate_cheikhs__username')
    inlines = (TalibAdminInline, MonthlyAwardRulesInlineAdmin, GoalInline)


class WonAwardsInlineAdmin(admin.TabularInline):
    model = WonAward


class TalibAdmin(ImportExportModelAdmin):
    #inlines = (WonAwardsInlineAdmin,PendingRewardPointsInline)
    exclude=('game','last_tgt_upd_suggest')
    readonly_fields=('last_dashboard_view_day',)


class PlannedGoalAdmin(admin.ModelAdmin):
    model = PlannedGoal
    actions = ('mark_acheived',)
    readonly_fields=('acquired_points',)
    search_fields=('goal__name', 'goal__halka__name', 'talib__user__username')
    
    def mark_acheived(self,request,queryset):
        for pgoal in queryset:
            pgoal.mark_acheived(3)
    mark_acheived.short_description = "Mark goal acheived with medium quality"


class GoalAdmin(ImportExportModelAdmin):
    model=Goal
    list_display = ('halka', 'name' ,'goal_award', 'icon_pack')
    list_filter = ('halka',)
    search_fields=('name', 'halka__name')

admin.site.register(Halka, HalkaAdmin)
admin.site.register(Talib, TalibAdmin)
admin.site.register(PlannedGoal, PlannedGoalAdmin)
admin.site.register(AwardDefinition)
admin.site.register(Goal, GoalAdmin)
admin.site.register(AwardCounter)
admin.site.register(Sohba)
admin.site.register(SelfReview)