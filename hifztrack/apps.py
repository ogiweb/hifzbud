from django.apps import AppConfig


class HifztrackConfig(AppConfig):
    name = 'hifztrack'
