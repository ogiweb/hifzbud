import csv
from io import StringIO

from django.contrib.auth.models import User
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from secret_word.models import SecretWordChallange, SecretWordChallengeTemplate
from ..models import Halka, Talib
from ..serializers import HalkaSerializer

class CanChangeOrAddHalka(permissions.BasePermission):
    """
    Custom permission to only allow users with specific permissions to edit or add Halka.
    """

    def has_permission(self, request, view):
        # Check for permission to add or change Halka
        if request.method in permissions.SAFE_METHODS:
            # Always allow GET, HEAD, OPTIONS requests.
            return True
        elif request.method == 'POST':
            return request.user.has_perm('hifztrack.add_halka')
        elif request.method in ['PUT', 'PATCH']:
            return request.user.has_perm('hifztrack.change_halka')
        else:
            return False

class HalkaListCreateAPIView(generics.ListCreateAPIView):
    queryset = Halka.objects.all()
    serializer_class = HalkaSerializer
    permission_classes = [permissions.IsAuthenticated, CanChangeOrAddHalka]

class HalkaDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Halka.objects.all()
    serializer_class = HalkaSerializer
    permission_classes = [permissions.IsAuthenticated, CanChangeOrAddHalka]


class CSVUserUpload(APIView):
    permission_classes = [permissions.IsAuthenticated, CanChangeOrAddHalka]

    def post(self, request, *args, **kwargs):
        csv_file = request.body.decode('utf-8')

        # Detect delimiter
        sniffer = csv.Sniffer()
        dialect = sniffer.sniff(csv_file.splitlines()[0])

        reader = csv.DictReader(StringIO(csv_file), dialect=dialect)

        for row in reader:
            self.process_row(row)

        return Response("Users and Talibs created/updated successfully.", status=200)

    def process_row(self, row):
        username = row.get('username')
        password = row.get('password')
        first_name = row.get('first_name', '')
        last_name = row.get('last_name', '')
        halka_id = row.get('halka_id', '')
        challange_template = row.get('challange_template_id', '')

        user, created = User.objects.get_or_create(username=username,
                                                   defaults={'first_name': first_name, 'last_name': last_name})

        if password:
            user.set_password(password)
            user.save()

        if halka_id:
            halka = Halka.objects.get(id=halka_id)
            Talib.objects.get_or_create(user=user, halka=halka)

        if challange_template:
            SecretWordChallange.objects.get_or_create(
                user=user,
                template=SecretWordChallengeTemplate.objects.get(pk=challange_template))
