import random

from django.shortcuts import render, get_object_or_404, redirect
from django.http.response import JsonResponse, HttpResponseForbidden,\
    HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils.functional import cached_property
from django.views.generic.base import TemplateView, View
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from hifztrack.models import Halka, Goal, GoalNote, Talib, PlannedGoal, PGStatus
from datetime import date, timedelta, datetime
from django.db.models import Q
from django.db.models.aggregates import Count, Sum, Max, Min
from jchart import Chart
from django.db.models.expressions import OuterRef, Subquery
from django.urls.base import reverse
from django.utils.http import urlencode
from django.utils.translation import gettext as _
from hifztrack import utils
import calendar
import json
import qurandata
from django.db.models.functions.datetime import TruncDate, TruncMonth
from jchart.config import Axes
from hifztrack.models import Sohba, SelfReview, PendingRewardPoints
from dateutil.relativedelta import relativedelta
from django.core.exceptions import PermissionDenied, ValidationError
from django.conf import settings
from qurandata.models import Aya
import xlsxwriter
from io import BytesIO
from django.contrib.auth.models import User
from hifztrack import utils
from collections import OrderedDict


# Create your views here.


@login_required
def api_get_notes_by_goal(request,goalpk,talibuser):
    """
        GET query parameters :
        - count : number of notes to extract.
        - skip : number of notes to skip.
        - output: 'json' or 'html' (default json)
    """
    g = get_object_or_404(Goal,pk=goalpk)
    talib = Talib.objects.filter(halka = g.halka, user__username=talibuser).get()
    show_cheikh_notes = g.halka.ischeikh(request.user) 
    #Only halka cheikh or talib himself can fetch notes.
    if not show_cheikh_notes and not talib.user == request.user:
        return HttpResponseForbidden()
    return __api_list_notes(GoalNote.objects.filter(goal=g, talib=talib), request, show_cheikh_notes)

@login_required
def api_get_notes_by_talib(request,halkapk,talibuser):
    """
        GET query parameters :
        - count : number of notes to extract.
        - skip : number of notes to skip.
        - output: 'json' or 'html' (default json)
    """
    talib = Talib.objects.filter(halka__pk = halkapk, user__username=talibuser).get()
    show_cheikh_notes = talib.halka.ischeikh(request.user) 
    #Only halka cheikh or talib himself can fetch notes.
    if not show_cheikh_notes and not talib.user == request.user:
        return HttpResponseForbidden()
    return __api_list_notes(GoalNote.objects.filter(talib=talib), request, show_cheikh_notes, True)

def __api_list_notes(qry, request, show_cheikh_notes,show_goal_link=False):
    pagesize = int(request.GET.get('count','10'))
    skip = int(request.GET.get('skip','0'))
    if not show_cheikh_notes:
        qry = qry.filter(private_to_cheikh=False)
    notes = qry.order_by('-create_date')[skip:(skip+pagesize-1)]
    
    out = request.GET.get('output','json').lower()
    
    if out == 'html':
        return render(request, 'talib/ajax/pg_notes.html', context={'notes':notes,'showgoallink':show_goal_link})
    else:
        ret = []
        for note in notes:
            ret.append({
                'pk':note.pk,
                'day': note.create_date,
                'author': (note.author.get_full_name() or note.author.username),
                'content': note.content,
                'private_to_cheikh': note.private_to_cheikh
            })
        
        data = {
            "count":qry.count(),
            "notes":ret
        }
        return JsonResponse(data)


@login_required
def api_get_ayasel_by_goal(request,goalpk):
    g = get_object_or_404(Goal,pk=goalpk)
    
    return render(request, 'talib/ajax/ayaselector.html', 
                  context={
                      'ayarefs':qurandata.refs_from_text(g.name),
                      'suraname': (lambda suraidx: qurandata.models.Sura.objects.get(index=suraidx).name),
                      'ishighlighted': lambda suraidx,ayaidx: False
                    })
    

def ischeikh(user):
    return Halka.objects.filter(Q(cheikh=user) | Q(associate_cheikhs=user)).exists()


@login_required
def homepage(request):
    if ischeikh(request.user):
        return redirect('cheikh_home')
    
    h=Halka.objects.filter(talib__user=request.user).first()
    if h is not None:
        return redirect('dashboard',pk=h.pk)
    
    return render(request,'base.html', context={"messages":[_("You have no halka currently")]})

    
class POSTActionMixin: 
    """ The view with this accepts calls with following parameters :
    action=do-something&x=y....
    It calls the method action_do_something with the rest of parametres
    Note that dash (-) in <do-something> is replaced by '_' in method name"""
    
    def post(self, *args, **kwargs):
        actionmethod = getattr(self,'action_'+self.request.POST['action'].replace('-','_'))
        akwargs = dict((key,value) for key,value in self.request.POST.items() if key != 'action')
        ret = actionmethod(**akwargs)
        if isinstance(ret, HttpResponse):
            return ret
        return JsonResponse(ret or {})


@login_required
def dashboard(request):
    h=Halka.objects.filter(talib__user=request.user).first()
    if h is not None:
        return redirect('dashboard',pk=h.pk)
    
    return render(request,'base.html', context={"messages":[_("You have no halka currently")]})

def _gamified_state(talib, goal, day):
    if PendingRewardPoints.objects.filter(talib=talib, goal=goal,
                                          date__date__lte=day).exists():
        return "rewardable"
    pgqs = PlannedGoal.objects.filter(goal=goal, talib=talib, day__lte=day)
    if not pgqs.exists():
        return "locked"
    stt = pgqs.order_by('-day').first().status
    if stt == PGStatus.MISSED: return "sealed"
    if stt == PGStatus.INPROGRESS: return "unlockable"
    if stt == PGStatus.ACHEIVED:
        # AS we have no pending points
        return "rewarded"

class HalkaDashboardView(LoginRequiredMixin, UserPassesTestMixin, POSTActionMixin, TemplateView):
    template_name ="talib/dashboard.html"
    isadmin = False
    """ Cheikh mode will not suppose talib is myself"""
    cheikh_mode=False
        
    def test_func(self):
        self.halka = get_object_or_404(Halka, pk=self.kwargs['pk'])
        if 'talib' in self.kwargs:
            username=self.kwargs['talib']
        else:
            username=self.request.user.username
        self.talib = Talib.objects.filter(user__username=username, halka__pk=self.kwargs['pk']).first()
        if self.talib == None: return False
        if self.halka.ischeikh(self.request.user):
            self.isadmin=True
            return True
        if self.talib.user == self.request.user:
            self.isadmin=False
            return True
        return False

    def hist_chart(self):
        return TalibProgressChart(self, displaylegend=False, hideexrcices=False, hideYLabels=True, axeFontSize=8)
    
    class TodayChart(Chart):
        chart_type = 'doughnut'
        options = { 'cutoutPercentage' : 80}
        def __init__(self, stats):
            super().__init__()
            
            planned = stats['planned'] or 0
            expected_today = stats['expected_today'] or 0
            acquired = stats['acquired'] or 0
            self.data = [acquired, 
                         expected_today-acquired if acquired < expected_today else 0, 
                         planned-expected_today if acquired < expected_today else (planned-acquired if acquired < planned else 0)]
            
        def get_labels(self, **kwargs):
            return ['Acquired','Late','Remaining']
        
        def get_datasets(self, **kwargs):
            return [{
                'label': "Your progress",
                'backgroundColor': ["#1e7e34","yellow","white"],
                'data': self.data
                }
            ]
        
    def today_chart(self):
        return self.TodayChart(self.today_stats)
    

        
    def enrich_goal(self,goal, day):
        goal.status = _gamified_state(self.talib, goal, day)
        goal.last_note = GoalNote.objects.get(pk=goal.last_note) if goal.last_note is not None else None
        return goal
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        day = date.today()
        self.day1 = date(day.year,day.month,1)
        self.daylast = utils.last_day_of_month(day.year, day.month)
        self.today = day

        if not self.isadmin and (self.talib.last_dashboard_view_day != self.today):
            self.talib.last_dashboard_view_day = self.today
            self.talib.save()

        self.goals = list(self.enrich_goal(goal, day) for goal in Goal.qs_active_at(day).filter(halka__pk=self.kwargs['pk'])
                .annotate(last_note=Subquery(GoalNote.objects.filter(goal=OuterRef('pk'), talib=self.talib).order_by('-create_date').values('pk')[:1]))
                .all())
        self.today_stats = {
            "planned":self.talib.range_points_target(self.day1, self.daylast),
            "acquired": self.talib.range_points_acquired(self.day1, self.daylast),
            "expected_today": self.talib.range_points_target(self.day1, self.today)
        }
        
        self.month_label = '%s %d' % (calendar.month_name[self.today.month], self.today.year)
        if self.cheikh_mode:
            self.all_user_halkas = Halka.objects.filter(talib__user=self.talib.user)\
                        .filter(Q(cheikh=self.request.user) | Q(associate_cheikhs=self.request.user))\
                        .distinct()
        else:
            self.all_user_halkas = Halka.objects.filter(talib__user=self.talib.user)
        
        self.won_awards_per_goal=dict(
            (award.win_goal, award) for award in self.talib.get_active_won_awards() 
                                       if award.win_goal != None
        )
        
        return context

    @cached_property
    def challengers(self):
        #List talibs "around this one" to compare
        all = list(self.halka.talib_set.annotate(
                curr_points=Sum("game__pointchange__amount",
                filter=Q(game__pointchange__time__range=(self.day1, self.daylast + timedelta(days=1))))
            ).values('pk', 'curr_points', 'user__username').order_by('-curr_points'))
        pos = next(pos for (pos, c) in enumerate(all) if self.talib.pk == c['pk'])
        nc = (settings.HB_CHALLENGER_TALIBS_COUNT - 1)

        pos = max(0, pos-(nc//2))
        return all[pos:pos+(nc+1)]
    
    def action_reward(self, gpk):
        goal = get_object_or_404(Goal, pk=gpk)
        upgrades, points = self.talib.reward_pending_points([goal], date.today())
        lu = upgrades[0] if upgrades else None
        (month_points,_,pct_after)= self.talib.get_stats()
        if self.halka.show_next_award:
            na = self.halka.month_award_rules.filter(unlock_percent__gt = pct_after).order_by('unlock_percent').first()
        else:
            na = None
        return {
            "wonPoints": points,
            "monthPoints": month_points,
            "awards": [{"name": l.definition.name, "icon": l.definition.icon_html, "goal": l.win_goal.id if l.win_goal else None} for l in self.talib.get_won_awards()],
            "next_award": {"name": na.definition.name, "icon": na.definition.icon_html, "anchor": na.unlock_percent} if na is not None else None,
            "new_award": {"name": lu.name, "icon": lu.icon_html } if lu != None else None
        }

class TalibProgressChart(Chart):
    chart_type = "scatter"
    responsive = True
    maintainAspectRatio = False
    animation = False
    
    
    def __init__(self, view, displaylegend=True, hideexrcices=True, hideYLabels=False, axeFontSize=12):
        super().__init__()
        self.legend = {
            'display': displaylegend
        }
        self.hideexrcices = hideexrcices
        
        self.view = view
        d = self.view.day1
        self.ndays = calendar.monthrange(d.year, d.month)[1]
        #Score won related to goals
        self.goalscores = [None]*self.ndays
        
        #SCore won in total include goals, but also bonus and exercises.
        self.totalscores=[None]*self.ndays
        
        #velocity = points per day won
        tgt_velocity=(self.view.talib.points_month_tgt*1.0/self.ndays)
        
        self.target_scores = [tgt_velocity]+[None]*(self.ndays-2)+[self.view.talib.points_month_tgt]
        
        #Daily score achieved through planned goals (not including bonus  and exercies)
        daily_goalscore = (PlannedGoal.objects.filter(goal__halka=view.halka, talib=view.talib, day__range=(view.day1, view.daylast), acheived=True)
                   .values('day').annotate(awarded=Sum("goal__points_value"))\
                   .annotate(rewardable=Sum('pendingrewardpoints__point_count'))\
                   .order_by('day')).all()
        
        #Daily score rewarded to talib in total
        daily_won_points=(view.talib.game.pointchange_set.filter(time__date__range=(view.day1, view.daylast))
                                         .annotate(day=TruncDate('time')).values('day').annotate(won=Sum('amount'))).all()

        cumul=0
        lastday=d
        for ds in daily_goalscore:
            if ds['awarded'] == 0: continue 
            cumul=cumul+(ds['awarded'] or 0)
            lastday = ds['day']
            self.goalscores[(lastday-d).days]=cumul
            
        drp = dict( ((ds['day']-d).days,ds['rewardable'] or 0) for ds in daily_goalscore)
        dwp = dict( ((ds['day']-d).days,ds['won'] or 0) for ds in daily_won_points )
        
        tscumul=0
        for di in sorted(set(drp.keys()).union(dwp.keys())):
            tst = drp.get(di,0) + dwp.get(di,0)
            if tst == 0: continue
            tscumul=tscumul+tst
            self.totalscores[di]=tscumul
        
        today = date.today()
        is_curr_month = (today.month == d.month and today.year == d.year)
        
        if is_curr_month:
            # We are in current month, ensure there is a datapoint for today
            if lastday < today: lastday = today
        else:
            lastday = view.daylast
        self.goalscores[(lastday-d).days]=cumul
        self.totalscores[(lastday-d).days]=tscumul
        # Compute the suggested target
        if lastday != d:
            self.suggested_tgt = int((cumul * 1.0 / (lastday-d).days) * (view.daylast - d).days)+1
        else:  
            self.suggested_tgt = self.view.talib.points_month_tgt
        
        # Compute projected curve
        if lastday != view.daylast:
            self.proj = [None]*self.ndays
            self.proj[(lastday-view.day1).days]=cumul
            self.proj[self.ndays-1]= cumul + tgt_velocity * (view.daylast - lastday).days
        else:
            self.proj = None
        
        #target suggestion logic
        if is_curr_month: 
            #perf = delta with expected points today in pct of target points
            self.tgt_delta = cumul - (lastday-d).days * tgt_velocity
            relative_perf = self.tgt_delta / self.view.talib.points_month_tgt
            self.alert = len(daily_goalscore) > 2 and (relative_perf > 0.2 or relative_perf < -0.2)
        else:
            self.alert = False
            
        self.awards_ds=[{'x': self.ndays, 'y':ar.unlock_percent*view.talib.points_month_tgt/100.0, 'xLabel': ar.definition.name} for ar in view.halka.month_award_rules.all()]
        self.scales = {
                'xAxes':[Axes(type='linear',
                              position='bottom',
                            ticks={
                                'min':1,
                                'max':self.ndays,
                                'stepSize':1,
                                'fontSize': axeFontSize
                            })]
            }
        if hideYLabels:
            self.scales['yAxes']=[Axes(type='linear',
                              position='left',
                            ticks={
                                'display':False
                            })]
    def _asxy(self, data):
        return [{'x':i+1, 'y':y} for (i,y) in enumerate(data) if y != None]
    
    def get_datasets(self, **kwargs):
        ret = [{
            'label': _("Target"),
            'spanGaps': True,
            'borderColor': '#cccc00',
            'fill': False,
            'data': self._asxy(self.target_scores)
        },{
            'label': _("Goals"),
            'spanGaps': True,
            'borderColor': 'green',
            'backgroundColor': '#CAF183',
            'data': self._asxy(self.goalscores)
        },{
            'label': _("Exercice"),
            'spanGaps': True,
            'borderColor': 'orange',
            'backgroundColor': '#FADFD0',
            'data': self._asxy(self.totalscores),
            'hidden': self.hideexrcices
        },{
            'label': _("Award"),
            'fill': False,
            'showLine': False,
            'pointStyle':'triangle',
            'pointBackgroundColor':'gold',
            'pointBorderColor':'black',
            'pointRadius':5,
            'pointBorderWidth':1,
            'data': self.awards_ds,
            'hidden': True
            }
        ]
        
        if self.proj != None:
            ret.append({
                'label': _("Trend"),
                'spanGaps': True,
                'borderColor': 'green',
                'borderDash': [10,5],
                'fill': False,
                'data': self._asxy(self.proj)
            })
        
        return ret



class MonthlyHistoryChart(Chart):
    chart_type = 'bar'
    
    def __init__(self, firstmonth, monthly_score, suggested_target):
        super().__init__()
        self.scores = monthly_score
        self.plans = [suggested_target] + [None]*(len(monthly_score)-2) + [suggested_target]
        self.firstmonth = firstmonth
        
    def get_labels(self):
        return list((self.firstmonth + relativedelta(months=i)).strftime('%b') for i in range(0,len(self.scores)))

    def get_datasets(self, **kwargs):
        return [{
            'label': "Month Total",
            'backgroundColor':'#1e7e34',
            'data': self.scores
        },{
            'label': "Suggested Target",
            'data': self.plans,
            'borderColor':'yellow',
            'backgroundColor':'yellow',
            'type': 'line',
            'animation': False,
            'fill': False,
            'spanGaps': True
        }]
    
class HalkaMonthOverview(LoginRequiredMixin, UserPassesTestMixin, POSTActionMixin, TemplateView):
    template_name ="talib/halkamonth.html"
    
        
    def test_func(self):
        """ Access control """
        self.halka = Halka.objects.prefetch_related('talib_set').filter(pk=self.kwargs['pk']).first()
        self.talib = Talib.objects.filter(user__username=self.kwargs['talib'], halka__pk=self.kwargs['pk']).first()
        if (self.halka == None or self.talib == None): return False
        if self.halka.ischeikh(self.request.user):
            if self.talib.user == self.request.user:
                self.talib_halkas = Halka.objects.filter(talib__user__username=self.kwargs['talib'])
            else:
                self.talib_halkas = Halka.objects.filter(talib__user__username=self.kwargs['talib'])\
                        .filter(Q(cheikh=self.request.user) | Q(associate_cheikhs=self.request.user))\
                        .distinct()
            
            self.isadmin=True
            return True
        if self.talib.user == self.request.user:
            self.talib_halkas = Halka.objects.filter(talib__user__username=self.kwargs['talib'])
            self.isadmin=False
            return True
        
        return False
    
    def post(self, *args, **kwargs):
        if self.isadmin or self.request.POST['action'].startswith('byuser') or self.request.POST['action'] == 'reward':
            return super().post(*args, **kwargs)
        else: #Only admin can perform post actions
            raise PermissionDenied
    
    ## Main view methods
    
    def days(self):
        day = self.day1
        oneday = timedelta(days=1)
        while day.month == self.day1.month:
            yield day
            day += oneday
            
    def get_month_name(self, month):
        return calendar.month_name[month]
    
    def enrich_goal(self,goal):
        goal.planned={}
        for pg in goal.plannedgoal_set.filter(talib=self.talib, 
                                              day__range=(self.day1,self.daylast)):
            goal.planned[pg.day]=pg
        #fixime do I reallu need tyhs ?
        return goal
    
    def review_goalpks_cs(self, rev):
        return ','.join(str(x['pk']) for x in rev.goals.values('pk')) 
    

    def _suggest_target_update(self):
        monthhist = []
        tday = date.today()
        tday = date(tday.year, tday.month, 1)
        fday = tday - relativedelta(months=settings.HB_MTU_RECENT_MONTH_COUNT)
        #Daily score achieved through planned goals (not including bonus  and exercies)
        monthly = PlannedGoal.objects.filter(talib=self.talib, day__range=(fday, tday - timedelta(days=-1)), acheived=True).annotate(month=TruncMonth('day')).values('month').annotate(awarded=Sum("goal__points_value")).order_by('month').all()
        monthly = dict((x['month'], x['awarded']) for x in monthly)
        monthhist = []
        for i in range(settings.HB_MTU_RECENT_MONTH_COUNT, 0, -1):
            month1 = tday - relativedelta(months=i)
            monthhist.append(monthly[month1] if month1 in monthly else 0)
        
        pts = list(filter(lambda x:x > 0, monthhist))
        pts.sort()
        if len(pts) == 0:
            self.mtu_suggested_tgt = self.talib.points_month_tgt
            self.mtu_reco = _("The current target is %d per month. Depending on talib performance we will suggest eventual adjustments in coming months.") % self.talib.points_month_tgt
            self.talib.last_tgt_upd_suggest = date.today()
            self.talib.save()
        else:
            points = max(pts[int(len(pts) / 2)], monthhist[-1])
            if points < settings.HB_MTU_NORMAL_MIN_PCT * self.talib.points_month_tgt:
                self.mtu_suggested_tgt = int(points / settings.HB_MTU_NORMAL_MIN_PCT) + 1
                self.mtu_reco = _("<h5>Acheivement largely under target</h5> Target reduced from %(curr_tgt)d to %(suggested_tgt)d.") % {'curr_tgt':self.talib.points_month_tgt, 'suggested_tgt':self.mtu_suggested_tgt}
            elif points > settings.HB_MTU_NORMAL_MAX_PCT * self.talib.points_month_tgt:
                self.mtu_suggested_tgt = int(points / settings.HB_MTU_NORMAL_MAX_PCT) + 1
                self.mtu_reco = _("<h5>Acheivement largely over target </h5> Target increased from %(curr_tgt)d to %(suggested_tgt)d.") % {'curr_tgt':self.talib.points_month_tgt, 'suggested_tgt':self.mtu_suggested_tgt}
            else:
                self.mtu_suggested_tgt = self.talib.points_month_tgt
                self.mtu_reco = _("<h5>Acheivement close to target</h5> The target is kept %d per month.") % self.talib.points_month_tgt
            #last month performance was high
        self.mtu_chart = MonthlyHistoryChart(tday - relativedelta(months=settings.HB_MTU_RECENT_MONTH_COUNT), 
            monthhist, self.mtu_suggested_tgt)
        self.talib.points_month_tgt = self.mtu_suggested_tgt
        self.talib.last_tgt_upd_suggest = date.today()
        self.talib.save()
            
    GOAL_RATING_NEEDED_FOR_TODAY="today"
    GOAL_RATING_NEEDED_FOR_TARGET="target"
    GOAL_RATING_OK="ok"
    GOAL_RATING_INACTIVE="inactive"
    
    def _enrich_goals_with_rating(self):
        tgt=self.talib.range_points_target(self.day1, self.today)
        curr=self.talib.range_points_acheived_no_bonus(self.day1, self.daylast)
        
        todaytgt=self.talib.range_points_target(self.today, self.today)
        todaycurr=self.talib.range_points_acheived_no_bonus(self.today, self.today)
        
        if curr >= tgt:
            for goal in self.goals:
                goal.rating=self.GOAL_RATING_OK if goal.isactive() else self.GOAL_RATING_INACTIVE
            return
        
        NONEDAY=date(1,1,1)
        dp=0
        
        for g in sorted(self.goals, key=lambda g: (g.lastacheived or NONEDAY, g.position)):
            if not g.isactive():
                g.rating=self.GOAL_RATING_INACTIVE
                continue
            
            if (todaycurr+dp) < todaytgt:
                g.rating=self.GOAL_RATING_NEEDED_FOR_TODAY
            elif (curr+dp) < tgt:
                g.rating=self.GOAL_RATING_NEEDED_FOR_TARGET
            else:
                g.rating= self.GOAL_RATING_OK
            dp+=g.points_value

    def get_goals_qs(self):
        return Goal.qs_active_at_month(self.day1).annotate(
            lastacheived=Max('plannedgoal__day', filter=Q(plannedgoal__talib=self.talib, plannedgoal__acheived=True)),
        ).filter(halka__pk=self.kwargs['pk']).order_by('position', 'pk').all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.today = date.today()
        self.day1 = date(self.kwargs['year'],self.kwargs['month'],1)
        self.daylast =  utils.last_day_of_month(self.kwargs['year'],self.kwargs['month'])
        self.monthname = self.day1.strftime("%m %Y")
        self.goals = list(self.enrich_goal(goal) for goal in self.get_goals_qs())
        self._enrich_goals_with_rating()
        
        self.progress_chart = TalibProgressChart(self)
        
        self.month_self_reviews = self.talib.selfreview_set.filter(checked=False, day__range=(self.day1,self.daylast)).all()
        
        if self.request.user == self.talib.user:
            self.all_user_halkas = Halka.objects.filter(talib__user=self.talib.user)
        
        if self.isadmin and self.talib.enable_monthly_target_update:
            thismonth = (self.today.month, self.today.year)
            self.monthly_tgt_update_show = thismonth == (self.day1.month, self.day1.year) and thismonth != (self.talib.last_tgt_upd_suggest.month, self.talib.last_tgt_upd_suggest.year)
            if self.monthly_tgt_update_show:
                self._suggest_target_update()
        return context
    
    
    def _rated_response(self,responsejson=None):
        if responsejson == None: responsejson={}
        if not hasattr(self,'goals'):
            self.today = date.today()
            self.day1 = date(self.kwargs['year'],self.kwargs['month'],1)
            self.daylast =  utils.last_day_of_month(self.kwargs['year'],self.kwargs['month'])
            self.goals=list(Goal.qs_active_at_month(self.day1)
                              .annotate(lastacheived=Max('plannedgoal__day', filter=Q(plannedgoal__talib=self.talib, plannedgoal__acheived=True)))
                              .filter(halka__pk=self.kwargs['pk']).all())
            self._enrich_goals_with_rating()
        responsejson['_rating']=list({"goal":goal.pk, "rating":goal.rating} for goal in self.goals)
        return responsejson
    
    ## Ajax actions
    
    def action_set_unplanned(self, goalpk, day):
        pg = self.talib.plannedgoal_set.filter(goal__pk=int(goalpk), day=day).first()
        if pg!=None: 
            pg.delete()
        return self._rated_response({"status":"unplanned"})
    
    def action_set_planned(self, goalpk, day):
        day=datetime.strptime(day, "%Y-%m-%d").date()
        pg = self.talib.plannedgoal_set.filter(goal__pk=int(goalpk), day=day).first()
        if pg != None:
            if not pg.acheived: 
                pg.quality_score=0
                pg.save()
                return {"status":"planned"}
            pg.delete() #so that we cascade points with it
        self.talib.plannedgoal_set.create(goal=Goal.objects.get(pk=int(goalpk)),day=day)
        return self._rated_response({"status":"planned"})
    
    def action_mark_done(self, goalpk, day=None, quality=None, acheived=True):
        day=datetime.today() if day is None else datetime.strptime(day, "%Y-%m-%d").date()
        pg,_ = self.talib.plannedgoal_set.get_or_create(goal=self.halka.goal_set.get(pk=goalpk), day=day)
        if quality is None:
            quality = int(quality) if quality else (pg.quality_score or 3)
        if acheived:
            pg.mark_acheived(quality)
        else:
            pg.quality_score = quality
            pg.save()
        return self._rated_response({"status":f"quality-{quality}",
                                     "day":day,
                                     "acheived": pg.acheived})
    
    def action_add_goal(self, name, points, description, position):
        if position=='auto':
            position=(self.halka.goal_set.aggregate(a=Max('position'))['a'] or 0) + 16
        self.halka.goal_set.create(name=name, points_value=int(points), description=description, position=position, start_day=date.today())
    
    def action_update_goal(self, goalpk, name, points, description, position):
        if position=='auto':
            ret = self.halka.goal_set.filter(pk=int(goalpk)).update(name=name, points_value=int(points), description=description)
        else:
            ret = self.halka.goal_set.filter(pk=int(goalpk)).update(name=name, points_value=int(points), description=description, position=position)
        if ret == 0: raise Exception("No such goal pk="+goalpk)
        
    def action_end_goals(self, goalspk, mode="today"):
        endday = utils.last_day_of_month(self.kwargs['year'],self.kwargs['month'])
        today = date.today() 
        if today < endday:
            endday = today
        
        if mode != 'today':
            endday = endday - relativedelta(months=1)
            endday = utils.last_day_of_month(endday.year, endday .month)
        qs = self.halka.goal_set\
                    .filter(pk__in=(int(pk) for pk in goalspk.split(',')));
        glist = list(qs);
        for talib in self.halka.talib_set.all():
            talib.reward_pending_points(goals=glist)
        
        qs.update(end_day=endday)
        
        
    def action_resume_goal(self, goalpk):
        ret = self.halka.goal_set.filter(pk=int(goalpk)).update(end_day=None)
        if ret == 0: raise Exception("No such goal pk="+goalpk)
        
    def action_add_many_goals(self, goals, points, pos):
        glist = goals.split('\n')
        
        if pos == 'start':
            pos=(self.halka.goal_set.aggregate(a=Min('position'))['a'] or 0) - 16.0 * len(glist)
        elif pos=='end':
            pos=(self.halka.goal_set.aggregate(a=Max('position'))['a'] or 0) + 16.0
        else:
            pos=float(pos)
            
            
        maxpos = self.halka.goal_set.filter(position__gt=pos)\
                            .aggregate(a=Min('position'))['a']
        
        posstep = (maxpos - pos) / (len(glist)+1) if maxpos != None else 16
            
        for goal in glist:
            s = goal.rsplit(',',1)
            if len(s) == 2: 
                g,p = s
                p = int(p.strip('"'))
            else:
                g = goal
                p = 0
                for ref in qurandata.refs_from_text(goal):
                    p += ref.count()
                if p == 0:
                    p = int(points)
        
            g = g.strip()
            g = g.strip('"')
                
            if g == '': continue
            self.halka.goal_set.create(name=g, points_value=p, description="", position=pos, start_day=date.today())
            pos += posstep
    
    def action_edit_goals(self, goals):
        glist = list(OrderedDict.fromkeys(goals.split('\n')))
        goals_by_name = dict((g.name, g) for g in self.halka.goal_set.all())
        
        #Start day for eventual new goals
        stday = utils.last_day_of_month(self.kwargs['year'],self.kwargs['month'])
        today = date.today() 
        if today < stday:
            stday = today
            
        for idx,goal in enumerate(glist):
            goal = goal.strip()
            goal = goal.strip('"')
            if not goal: continue
            if goal in goals_by_name:
                g = goals_by_name.pop(goal) 
                g.position=idx*10
                g.end_day=None
                if g.start_day != None and g.start_day > stday:
                    g.start_day=stday
                g.save()
            else:
                g = goal
                p = 0
                for ref in qurandata.refs_from_text(goal):
                    p += ref.count()
                if p == 0:
                    p = 10
                self.halka.goal_set.create(name=g, points_value=p, description="", 
                                           position=idx*10, 
                                           start_day=stday)
        for g in goals_by_name.values():
            idx = idx+1
            g.position = idx*10;
            g.save()
            
        return JsonResponse({"todelete":[g.pk for g in goals_by_name.values()
                                    if (g.start_day == None or g.start_day <= stday) \
                                        and (g.end_day == None or g.end_day > stday)]})
    
    def action_add_bonus(self, points):
        self.talib.add_points(points)
    
    def action_add_note(self, goalpk, content, cheikhonly, markdone, quality=None, day=None):
        goal = get_object_or_404(Goal, pk=int(goalpk), halka=self.halka)
        if content and content.strip():
            try:
                self.talib.goalnote_set.create(
                    goal=goal, author=self.request.user, content=content, private_to_cheikh = ('true' == cheikhonly.lower()))
            except qurandata.BadRef as e:
                return JsonResponse({ "error":_("Bad aya reference : %(ref)s (%(errcode)s)" % e.__dict__) }, status=500)
            
        if markdone.lower() == 'true' :
            return self.action_mark_done(goalpk, day or date.today().isoformat(), quality)
    
    def action_update_target(self, newval):
        newval = int(newval)
        if newval <= 0: raise Exception("bad value for target : must be strictly positive")
        self.talib.points_month_tgt = newval
        self.talib.save()
        #reload after sucess
    
    def action_use_counter(self, counterid, count):
        count=int(count)
        counterid=int(counterid)
        counter = self.talib.user.award_counters.filter(id=counterid).get()
        if counter.count < count: raise Exception("bad value: cannot use more items than current count (%d)" % counter.count)
        counter.count -= count;
        counter.save()
        
    def action_mark_review_checked(self, reviewpk):
        reviewpk = int(reviewpk)
        r= self.talib.selfreview_set.get(pk=reviewpk)
        r.checked=True
        r.save()
        
    def action_save_review_check_time(self, reviewpk, checktimesecs):
        checktimesecs = int(checktimesecs)
        reviewpk=int(reviewpk)
        r= self.talib.selfreview_set.get(pk=reviewpk)
        r.checking_current_time=checktimesecs
        r.save()
        
    def action_ayaselector(self, goalpk):
        g = get_object_or_404(Goal,pk=goalpk)
        lastnote=GoalNote.objects.filter(goal=g, talib=self.talib).order_by('-create_date').values('content').first()
        if lastnote:
            lastrefs = qurandata.refs_from_text(lastnote['content'])
        else:
            lastrefs = []
            
        def ishighlighted(suraidx, ayaidx):
            for aya in lastrefs:
                if aya.suraidx == suraidx and ayaidx >= aya.startaya and ayaidx <= aya.endaya: 
                    return True
            return False
        
        return render(self.request, 'talib/ajax/ayaselector.html', 
                  context={
                      'ayarefs':qurandata.refs_from_text(g.name),
                      'suraname': (lambda suraidx: qurandata.models.Sura.objects.get(index=suraidx).name),
                      'ishighlighted': ishighlighted
                    })
    
    def action_byuser_mark_done(self, goalpk, day=None, quality=None):
        return self.action_mark_done(goalpk, day, quality, acheived=False)

    def action_byuser_set_planned(self, goalpk, day):
        day=datetime.strptime(day, "%Y-%m-%d").date()
        pg = self.talib.plannedgoal_set.filter(goal__pk=int(goalpk), day=day).first()
        if pg == None: return;
        pg.quality_score = 0
        pg.save()
        return self._rated_response({"status":"planned"})


class HalkaCurrentMonthOverview(HalkaMonthOverview):
    def dispatch(self, request, *args, **kwargs):
        today = date.today()
        self.kwargs['month'] = today.month
        self.kwargs['year'] = today.year
        self.is_current=True
        return super(HalkaMonthOverview,self).dispatch(request, *args,**self.kwargs)


class HalkaProgressList(HalkaCurrentMonthOverview):
    template_name = "talib/halkalist.html"

    @cached_property
    def won_awards_per_goal(self):
        return dict(
            (award.win_goal, award) for award in self.talib.get_active_won_awards()
                                       if award.win_goal != None
        )

    def action_reward(self, gpk):
        goal = get_object_or_404(Goal, pk=gpk)
        upgrades, points = self.talib.reward_pending_points([goal], date.today())
        lu = upgrades[0] if upgrades else None
        (month_points,_,pct_after)= self.talib.get_stats()
        if self.halka.show_next_award:
            na = self.halka.month_award_rules.filter(unlock_percent__gt = pct_after).order_by('unlock_percent').first()
        else:
            na = None

        unlocked_challenges = False
        for u in upgrades:
            if u.unlock_challenge:
                unlocked_challenges=True
                break

        return {
            "wonPoints": points,
            "monthPoints": month_points,
            "awards": [{"name":l.definition.name, "icon":l.definition.icon_html, "goal": l.win_goal.id if l.win_goal else None} for l in self.talib.get_won_awards()],
            "next_award": {"name":na.definition.name, "icon":na.definition.icon_html, "anchor": na.unlock_percent} if na != None else None,
            "new_award": {"name":lu.name, "icon":lu.icon_html, "unlocked_challenges": unlocked_challenges } if lu != None else None
        }

    def enrich_goal(self,goal):
        miss_date = self.today - timedelta(days=goal.halka.miss_delay_days)
        if goal.lastacheived is not None and goal.lastacheived < miss_date :
            goal.lastacheived = None
        if goal.lasttalibscore is not None and goal.lasttalibscore < miss_date:
            goal.lasttalibscore = None

        if goal.lastacheived is not None and goal.lasttalibscore is not None:
            if goal.lastacheived > goal.lasttalibscore:
                goal.lasttalibscore=None
            else:
                goal.lastacheived=None
        if goal.lastacheived is not None:
            goal.status = _gamified_state(self.talib, goal, self.today)
        return goal

    def get_goals_qs(self):
        return super().get_goals_qs().annotate(
            lasttalibscore=Max('plannedgoal__day', filter=Q(plannedgoal__talib=self.talib, plannedgoal__acheived=False, plannedgoal__quality_score__gt=0))
        ).order_by('position')

    def random_animation(self):
        return random.choice(['zoomIn','zoomInDown','zoomInLeft','zoomInRight','zoomInUp','rotateIn',
                              'rotateInDownLeft','rotateInDownRight','rotateInUpLeft','rotateInUpRight',
                              'lightSpeedInRight','lightSpeedInLeft','flipInX','flipInY'])

def cheikh_home(request):
    mode = request.GET.get('mode') or request.session.get('cheikhhome.mode') or 'talib'
    request.session['cheikhhome.mode'] = mode
    return redirect(reverse('talib_list' if mode == 'talib' else 'halka_list'))
    
class CheikhHomePerHalka(LoginRequiredMixin, POSTActionMixin, TemplateView):
    template_name ="talib/cheikh_home_perhalka.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.managed_halkas = Halka.objects.filter(Q(cheikh=self.request.user) | Q(associate_cheikhs=self.request.user)).distinct().all()
        today = date.today()
        startofweek= today - timedelta(days=calendar.weekday(today.year, today.month, today.day))
        startofmonth = date(day=1, month=today.month, year=today.year)
        endofmonth = utils.last_day_of_month(today.year, today.month)
        startnmonth = endofmonth + timedelta(days=1)
        endnmonth = utils.last_day_of_month(startnmonth.year, startnmonth.month)
        
        self.downloads = [
            (_("This week"), startofweek.strftime("%Y-%m-%d"),""),
            (_("Next week"), (startofweek + timedelta(days=7)).strftime("%Y-%m-%d"),""),
            (_("In 2 weeks"), (startofweek + timedelta(days=14)).strftime("%Y-%m-%d"),""),
            (_("In 3 weeks"), (startofweek + timedelta(days=23)).strftime("%Y-%m-%d"),""),
            (_("This month"), startofmonth.strftime("%Y-%m-%d"),endofmonth.strftime("%Y-%m-%d")),
            (_("Next month"), startnmonth.strftime("%Y-%m-%d"),endnmonth.strftime("%Y-%m-%d"))
        ]
        return context

class CheikhHomePerTalib(LoginRequiredMixin, POSTActionMixin, TemplateView):
    template_name ="talib/cheikh_home_pertalib.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.managed_talibs = {}
        for talib in Talib.objects\
                        .filter(Q(halka__cheikh=self.request.user) | Q(halka__associate_cheikhs=self.request.user))\
                        .select_related('halka','user').distinct():
            if talib.user.username not in self.managed_talibs:
                self.managed_talibs[talib.user.username] = [talib]
            else:
                self.managed_talibs[talib.user.username].append(talib)
            
        today = date.today()
        startofweek= today - timedelta(days=calendar.weekday(today.year, today.month, today.day))
        startofmonth = date(day=1, month=today.month, year=today.year)
        endofmonth = utils.last_day_of_month(today.year, today.month)
        startnmonth = endofmonth + timedelta(days=1)
        endnmonth = utils.last_day_of_month(startnmonth.year, startnmonth.month)
        
        self.downloads = [
            (_("This week"), startofweek.strftime("%Y-%m-%d"),""),
            (_("Next week"), (startofweek + timedelta(days=7)).strftime("%Y-%m-%d"),""),
            (_("In 2 weeks"), (startofweek + timedelta(days=14)).strftime("%Y-%m-%d"),""),
            (_("In 3 weeks"), (startofweek + timedelta(days=23)).strftime("%Y-%m-%d"),""),
            (_("This month"), startofmonth,endofmonth),
            (_("Next month"), startnmonth,endnmonth)
        ]
        return context
    

class AwardWallView(LoginRequiredMixin, TemplateView):
    template_name ="talib/my_award_wall.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.talibs = Talib.objects.filter(user__username=self.request.user.username).all()
        self.year = self.kwargs['year']
        return context
    
    def get_full_name(self):
        return self.request.user.get_full_name() or self.request.user.username
    
    def get_last_day_of_month(self, month):
        return utils.last_day_of_month(self.year, month)
    
    def get_month_name(self, month):
        return calendar.month_name[month]
        
    def is_month_in_future(self, month):
        return date(self.year, month, 1) > date.today()

class GoalNoteExercise(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name ="talib/exercise.html"
    """ TODO: 
        => Show score of last goalnote on each goal in dashboard + button to exercise.
        => Show score one note itself ?
        => Track number of tries of a goalnote (and reduce reward after too much retries)
        => Add some fency audio.
        => Propose exercise on last non 100% acheived note not today ...
    """
    def test_func(self):
        """ Access control """
        self.ex = get_object_or_404(GoalNote, pk=self.kwargs['pk'])
        if self.ex.talib.user == self.request.user:
            self.cheikh=False
            return True
        elif self.ex.goal.halka.ischeikh(self.request.user):
            self.cheikh=True
            return True
        else:
            return False
    
    def get(self, *args, **kwargs):
        self.ex.ex_resume()
        
        if 'ischaining' not in self.request.GET:
            self.request.session['exercise.chainpos']=1
            self.request.session['exercise.chainsize']=int(self.request.GET['chain']) if ('chain' in self.request.GET) else 0
            self.request.session.pop('exercise.reward',None)
        
        return super().get(*args, **kwargs)
    
    def _suggest_next_ex(self):
        today = date.today()
        qs = GoalNote.objects.exclude(pk=self.ex.pk).filter(
            talib = self.ex.talib,
            goal__halka = self.ex.talib.halka,
            ex_step_count__gt = 0
        ).filter(
            ((Q(goal__start_day__isnull=True)|Q(goal__start_day__lte=today)) &
            (Q(goal__end_day__isnull=True)|Q(goal__end_day__gte=today)))
        )
        if settings.HB_MAX_EXERCICE_AGE_DAYS:
            qs = qs.filter(create_date__date__gte = today - timedelta(days=settings.HB_MAX_EXERCICE_AGE_DAYS))

        cand = qs.filter(ex_last_finished=None).order_by('-create_date').first()
        if not cand:
            cand = qs.order_by('ex_last_finished').first()
        return cand
    
    def post(self, *args, **kwargs):
        sn = int(self.request.POST['stepnum'])
        grade = int(self.request.POST['grade']) if ('grade' in self.request.POST) else None
        if sn != self.ex.ex_step or (self.ex.ex_step > self.ex.ex_step_count): 
            return super().get(*args, **kwargs)
        
        if not self.ex.ex_end_step(grade):
            return super().get(*args, **kwargs)
        
        #Accumulate with in-session reward
        exchain = self.request.session['exercise.chainpos']
        if exchain < self.request.session['exercise.chainsize']:
            nextex = self._suggest_next_ex()
            if nextex:
                self.request.session['exercise.chainpos'] = exchain+1
                return redirect(reverse('exercise', kwargs={'pk': nextex.pk})+"?ischaining=true")
        
        self.request.session.pop('exercise.chainpos',None)
        self.request.session.pop('exercise.chainsize',None)
        
        return redirect(self.get_end_url())
    
    def get_end_url(self):
        if self.cheikh:
            return reverse('cheikh_dashboard', 
                    kwargs={'pk': self.ex.talib.halka.pk, 
                            'talib':self.ex.talib.user.username })
        else:
            return reverse('dashboard', 
                    kwargs={'pk': self.ex.talib.halka.pk})
     
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.ayas = list(self.ex.ex_aya_refs[self.ex.ex_step-1].getayas())
        self.sura = self.ayas[0].sura
        
        a0 = self.ayas[0]
        if a0.index == 1: self.prev = None
        else: self.prev = Aya.objects.filter(sura=a0.sura, index=a0.index-1).get()
    
        a0 = self.ayas[-1]
        if a0.index == a0.sura.ayas: self.next = None
        else: self.next = Aya.objects.filter(sura=a0.sura, index=a0.index+1).get()
    
        return context

    def getprogress(self):
        rel = (self.ex.ex_step-1) * 1.0 / (self.ex.ex_step_count if self.ex.ex_step_count > 0 else 1)
        exchainsz = self.request.session.get('exercise.chainsize',0)
        if not exchainsz: return 100*rel;
        rel = (self.request.session['exercise.chainpos']-1+rel)/exchainsz
        return 100*rel;
    
@login_required
def sohba(request):
    f = Sohba.objects.filter(talibs__user=request.user).first()
    if f == None:
        return render(request,'base.html', context={"messages":[_("You have no Sohba currently")]})
    else:
        return redirect('sohba',pk=f.pk)
    
class SohbaProgressChart(Chart):
    chart_type = "line"
    responsive = True
    maintainAspectRatio = False
    
    def get_labels(self, **kwargs):
        today = date.today()
        ndays = calendar.monthrange(today.year, today.month)[1]
        return [str(i) for i in range(1, ndays+1)]
     
    def get_datasets(self, sohbapk, flattarget=False):
        self.today = date.today()
        self.day1 = date(self.today.year,self.today.month,1)
        self.daylast =  utils.last_day_of_month(self.today.year, self.today.month)
        self.ndays = calendar.monthrange(self.today.year, self.today.month)[1]
        
        if flattarget==True:
            tgt = [100]+[None]*(self.ndays-2)+[100]
        else:
            tgt = [100/self.ndays]+[None]*(self.ndays-2)+[100]
            
        ret = [{
            'label': _("Target"),
            'spanGaps': True,
            'borderColor': '#cccc00',
            'fill': False,
            'data': tgt
        }]
        
        colors=["#0072BB", "#FF4C3B", "#000000", "#C6C8CA","#44B3C2","#F1A94E","#E45641","#5D4C46","#7B8D8E","#F2EDD8" ]
        
        for i,talib in enumerate(get_object_or_404(Sohba, pk=sohbapk).talibs.all()):
            ret.extend(self.get_talib_datasets(talib, colors[i%len(colors)], flattarget))
        return ret
    
    def get_talib_datasets(self, talib, color, flattarget=False):
        totalscores=[None]*self.ndays
        daily_goalscore = (PendingRewardPoints.objects.filter(goal__halka=talib.halka, talib=talib, date__date__range=(self.day1, self.daylast))
                   .annotate(day=TruncDate('date')).values('day')
                   .annotate(rewardable=Sum("point_count"))
                   .order_by('day')).all()
        daily_won_points=(talib.game.pointchange_set.filter(time__date__range=(self.day1, self.daylast))
                                         .annotate(day=TruncDate('time')).values('day').annotate(won=Sum('amount'))).all()

            
        drp = dict( ((ds['day']-self.day1).days,ds['rewardable'] or 0) for ds in daily_goalscore)
        dwp = dict( ((ds['day']-self.day1).days,ds['won'] or 0) for ds in daily_won_points )
        
        tscumul=0
        
        
        for di in sorted(set(drp.keys()).union(dwp.keys())):
            tst = 100.0 * (drp.get(di,0) + dwp.get(di,0))/talib.points_month_tgt
            if tst == 0: continue
            tscumul=tscumul+tst
            totalscores[di]=tscumul
            
        
        ti = (self.today-self.day1).days
        totalscores[ti]=tscumul

        if flattarget: #FIXME: Could be simpler ? EAch day percent of acheivement of tgt ?
            for di in range(0,self.ndays):
                if totalscores[di] == None: continue
                totalscores[di] *= self.ndays/(1+di)
            
        ret = [{
            'label': str(talib),
            'spanGaps': True,
            'borderColor': color,
            'fill': False,
            'data': totalscores
        }]
        # Compute projected curve
        # if self.today != self.daylast:
        #   tgt_velocity=(talib.points_month_tgt*1.0/self.ndays)
        #   proj = [None]*self.ndays
        #   proj[(today-self.day1).days]=tscumul
        #   proj[self.ndays-1]= tscumul + tgt_velocity * (self.daylast - today).days
        #    ret.append({
        #       'label': _("Trend"),
        #       'spanGaps': True,
        #       'borderColor': 'green',
        #       'borderDash': [10,5],
        #       'fill': False,
        #       'data': self._asxy(self.proj)
        #   })
        
        return ret

class SohbaView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name ="talib/sohba.html"
    
    def test_func(self):
        self.sohba = get_object_or_404(Sohba, pk=self.kwargs['pk'])
        return self.sohba.talibs.filter(user=self.request.user).exists()
        
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.all_user_sohbas = Sohba.objects.filter(talibs__user=self.request.user).distinct()
        self.chart = SohbaProgressChart()
        if 'target' in  self.request.GET:
            self.request.session['sohba.chart.flattarget'] = (self.request.GET.get('target','ramp') == 'flat')
        self.isflat = self.request.session.get('sohba.chart.flattarget',False)
        return context

class SelfReviewView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name ="talib/selfreview.html"
    
    def post(self, *args, **kwargs):
        rev = SelfReview()
        rev.talib= self.talib
        rev.day = date.today()
        rev.save()
        for goalpk in self.request.POST.getlist('goal'):
            rev.goals.add(Goal.objects.get(pk=int(goalpk)))
        rev.audio = self.request.FILES['audio']
        rev.checkpoints = self.request.POST.getlist('checkpoint')
        rev.save()
        return JsonResponse({'reviewId':rev.pk})
    
    def test_func(self):
        self.halka = get_object_or_404(Halka, pk=self.kwargs['pk'])
        self.talib = Talib.objects.filter(user=self.request.user, halka__pk=self.kwargs['pk']).first()
        return self.talib != None
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        self.goals = (Goal.qs_active_at_month().filter(halka=self.halka).all())
        self.suggestedgoal = (Goal.qs_active_at_month().filter(halka=self.halka)
                              .annotate(lastacheived=Max('plannedgoal__day', filter=Q(plannedgoal__talib=self.talib)))
                              .order_by('lastacheived','position').first())
        self.end_url="#"
        return context
    
class TrackingXLSXSheet(LoginRequiredMixin, View):
    
    def get(self, request, *args, **kwargs):
        
        start_day = request.GET.get('startday')
        if not start_day:
            start_day = datetime.today()
            start_day = start_day - timedelta(days=
                    calendar.weekday(start_day.year, start_day.month, 
                                     start_day.day))
        else:
            start_day = datetime.strptime(start_day, "%Y-%m-%d").date()
        end_day = request.GET.get('endday')
        if not end_day:
            end_day = start_day + timedelta(days=6)
        else:
            end_day = datetime.strptime(end_day, "%Y-%m-%d").date()
            
        if end_day < start_day:
            raise ValidationError("Either do not specify startday and endday or endday > startday")
        
        if 'talib' in self.kwargs:
            tuser = User.objects.get(username=self.kwargs['talib'])
            talibs = Talib.objects.filter(user=tuser)\
                     .filter(Q(halka__cheikh=request.user) | 
                             Q(halka__associate_cheikhs=request.user))\
                     .distinct()
        else:
            tuser = request.user
            talibs = Talib.objects.filter(user=tuser)
    
        stream = BytesIO()
        workbook = xlsxwriter.Workbook(stream, {'in-memory': True, 'remove_timezone': True})
        worksheet = workbook.add_worksheet()
        date_format = workbook.add_format({'num_format': 'ddd dd/mm/yy'})
        bold = workbook.add_format({'bold':True})
        header_cell = workbook.add_format({'bold':True,'border':1,
                                           'text_wrap': True, 'bg_color':'#EEEEEE'})
        cell = workbook.add_format({'border':1,'text_wrap': True})
        date_header = workbook.add_format({'bold':True,'border':1,'bg_color':'#EEEEEE', 'num_format': 'ddd dd/mm/yy'})
        
        worksheet.write(0,0, _("Name"), bold)
        worksheet.write(0,1, tuser.get_full_name() or tuser.username)
        
        worksheet.write(1,0, _("Von"), bold)
        worksheet.write(1,1, start_day, date_format)
        
        worksheet.write(2,0, _("Bis"), bold)
        worksheet.write(2,1, end_day, date_format)
        
        talibs = list(talibs)
        
        
        row=5
        day = start_day
        ONEDAY = timedelta(days=1)
        
        while day <= end_day:
            worksheet.merge_range(row,0, row+1,0, day, date_header)
            worksheet.write(row,1, "Ziel", date_header)
            worksheet.write(row+1,1, "Erreicht", date_header)
            
            for c,t in enumerate(talibs):
                g = [utils.quranrefs2str(n) for n in PlannedGoal.objects.filter(talib=t, day=day)\
                            .values_list('goal__name',flat=True)]
                worksheet.write(row,c+2, "\n".join(g), cell)
                worksheet.set_row(row, height=max(15*len(g),15))
                
                worksheet.write(row+1,c+2, '', cell)
                worksheet.set_row(row+1, height=max(15*len(g),25))
                
                worksheet.merge_range(row, len(talibs)+2, row+1, len(talibs)+2, 
                                      '', cell)
                
            row = row + 2
            day = day + ONEDAY
        
        
        headers = [_("Tag"),_("")]
        headers.extend(utils.quranrefs2str(t.halka.name) for t in talibs)
        headers.append(_("Studenten unterschrift"))
        
        for c,h in enumerate(headers):
            worksheet.write(4, c, h, header_cell)
        
        worksheet.set_column(0, 1, 15)
        worksheet.set_column(2,len(talibs)+1, 20)
        worksheet.set_column(len(talibs)+2,len(talibs)+2, 10)
            
        
        workbook.close()
        response = HttpResponse(stream.getvalue(), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=hifzo-%s-%s.xlsx' % (tuser.username, start_day.strftime("%d/%m/%y"))
        return response