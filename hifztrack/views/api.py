'''
Created on Apr 10, 2020

@author: omar
'''
from django.http.response import JsonResponse
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import authentication_classes, api_view
from rest_framework.authentication import BasicAuthentication
from hifztrack.models import Talib, Halka, AwardDefinition, Goal
from django.db.models import Q
from django.urls.base import reverse
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.contrib.auth.models import User
import re
import datetime
from django.conf import settings

@api_view(['GET','POST'])
@authentication_classes([BasicAuthentication])
def api_sync_ayaranges(request):
    
    def talib_to_json(talib):
        tname = talib.get_full_name()
        if talib.halka.name != tname:
            tname = tname + "("+talib.halka.name+")"
        return { "serverKey":str(talib.pk), 
               "talibName": tname,
               "talibUrl": request.build_absolute_uri(reverse('curr_month_overview',
                                            kwargs={'pk':talib.halka.pk, 
                                                    'talib': talib.user.username}))}
    
    def find_goal(goals, sura, ayah):
        for goal in goals:
            for ref in goal.ayarefs:
                if ref.suraidx != sura: continue
                if ref.fullsura: return goal
                if ref.startaya <= ayah <= ref.endaya: return goal
            
        
    if request.method == 'GET':
        talibs = Talib.objects\
                        .filter(Q(halka__cheikh=request.user) | Q(halka__associate_cheikhs=request.user))\
                        .select_related('halka','user').distinct()
        return JsonResponse([ talib_to_json(talib) for talib in talibs], 
                            safe=False)
    elif request.method == 'POST':
        talib = None
        # Try to find talib by serverKey
        if request.data.get('serverKey'):
            try:
                talib = Talib.objects\
                        .filter(Q(halka__cheikh=request.user) | Q(halka__associate_cheikhs=request.user))\
                        .filter(pk=int(request.data['serverKey'])).first()
            except ObjectDoesNotExist:
                pass
        
        #Try to create halka and talib
        if talib is None:
            talib_name = request.data.get('talibName')
            if not talib_name: 
                raise ValidationError("No valid serverKey nor talibName")
            
            halka, created = Halka.objects.get_or_create(cheikh=request.user, name=talib_name)
            if created:
                for a in settings.HB_HALKA_DEFAULT_AWARDS:
                    try:
                        ad = AwardDefinition.objects.get(name=a['name'])
                    except ObjectDoesNotExist:
                        continue
                    halka.month_award_rules.create(
                        definition = ad,
                        unlock_percent=a['percent'])

            talib=halka.talib_set.filter(user__first_name=talib_name).first()
            if talib == None:
                user = User.objects.filter(username='__user%d__'%halka.pk).first()
                if user == None:
                    user = User.objects.create_user('__user%d__'%halka.pk, '', '')
                    user.first_name=request.data.get('talibName','')[0:30]
                    user.save()
                    
                talib = Talib.objects.create(halka=halka, user=user)
        else:
            halka=talib.halka
        goals = list(Goal.qs_active_at().filter(halka=halka))
        for r in request.data['reviewRanges']:
            if 'sura' not in r: continue #Do not try to process ranges that are not fully qualified
            g = find_goal(goals, r['sura'], r['startAyah'])
                
            """=> Optimize halqamonth queries.
            TODO: fix exo
            TODO: Quality 5. if 0 stroke in page.
                  Quality 4. if not 0
                  Quality 3 if max(overpages)(sum(strength)) > 3
            """
            if g is None:
                page = r['page']
                ref = ('S%(sura)dP%(page)d' % r) if page < 553 else ('S%(sura)d' % r)
                g=halka.goal_set.create(position=1000+page*200+r['sura'], name=ref)
                goals.append(g)
                        
            day=datetime.datetime.strptime(r['date'], "%Y-%m-%d").date()
            pg,_=talib.plannedgoal_set.get_or_create(goal=g, day=day)
            pg.mark_acheived(5)
            talib.goalnote_set.create(
                goal=g, author=request.user, 
                content='S%(sura)dA%(startAyah)dW%(startWord)d:A%(endAyah)dW%(endWord)d' % r)
            
        return JsonResponse(talib_to_json(talib))

