function makeRevealerHtml(text, visiblewords, url) {
	//visibleword : when positive => Number of shown words in prefix, when negative suffix
	var words = text.split(/\s/);
	var $ret = $('<a class="revealer" target="_blank"></a>');
	var pfx="", sfx="";
	
	if(visiblewords > 0)
		pfx=words.splice(0,visiblewords).join(" ")+" ";
	else if(visiblewords < 0) {
		visiblewords = -visiblewords;
		sfx=" "+words.splice(words.length-visiblewords, visiblewords).join(" ")
	}
	
	$ret.html(pfx);
	if(url) $ret.attr("href",url);
	var $w = $('<span class="revealer-word"></span>')
	$w.html(words.join(" "));
	$ret.append($w);
	$ret.append(sfx);
	return $ret[0].outerHTML;
}
$(function(){
	$("body").delegate(".revealer","click",function(){
		if($(this).hasClass("revealed")) $(this).removeClass("revealed");
		else $(this).addClass("revealed");
	})
	
	$("body").delegate(".quranref","click", function(){
		var $t = $(this);
		$t.popover({
			title:"",
			content:'<i class="fas fa-spinner fa-spin"></i>',
			html: true
		});
		
		$t.popover('show');
		var pop = $t.data('bs.popover');
		
		function ajaxLoadAya(suraidx, ayaidx){
			pop.config.content='<i class="fas fa-spinner fa-spin"></i>';
			$t.popover('show');
			
			$.ajax({
				url: qurandata_api_url+"sura/"+suraidx+"/aya/"+ayaidx+"/info",
				success: function(res){
					pop.config.title='<div dir="rtl">﴿'+res.sura.index+"﴾"+res.sura.title+"</div>";
					pop.config.content='<div class="qr-aya" dir="rtl">'+
												makeRevealerHtml(res.text,2, res.url)+
												'﴿'+res.index+'﴾</div>'+
												'<div> <a class="goto-aya" data-sura="'+res.sura.index+'" data-aya="'+(res.index+1)+'" href="#"><i class="fas fa-chevron-circle-left"></i></a>'+
												'<a class="goto-aya" data-sura="'+res.sura.index+'" data-aya="'+(res.index-1)+'" href="#"><i class="fas fa-chevron-circle-right"></i></a></div>';
					$t.popover('show');
				},
				error: function(res){
					$t.popover('hide');
				}
			})
		}
		
		$(pop.tip).delegate('.goto-aya','click', function(){
			ajaxLoadAya($(this).data('sura'), $(this).data('aya'));
		});
		
		if($t.data('aya')) {
			ajaxLoadAya($(this).data('sura'), $(this).data('aya'));
		}
		
		
	});
	
	$("body").delegate(".quranref","shown.bs.popover", function(){
		var $t = $(this);
		var hidePopover = function(evt){
			if($(evt.target).is(".popover *")) { //Ignore clics in popovers
				$(document).one('click', hidePopover);
				return;
			}
			$t.popover('hide');
		};
		$(document).one('click', hidePopover);
	});

	$(".page-loaded").removeClass("page-loaded");
	$(".page-loading").remove();
	

	/** aya selector **/
	$("body").delegate(".ayasel-aya", "click", function(){
		$(this).toggleClass("ayasel-selected");
		$(this).next().toggleClass("ayasel-after-selected");
		$(this).prev().toggleClass("ayasel-before-selected");
	})
	
	$("body").delegate(".link-select","change",function(){
        var url = $(this).val();
        if(url) location.href=url;
    });
})

function ayasel2refs($selector) {
	var ret = "";
	$selector.find(".ayasel-selected:not(.ayasel-after-selected)").each(function(){
		var r="S"+$(this).data("suraidx")+"A"+$(this).data("ayaidx");
		if($(this).hasClass("ayasel-before-selected")) {
			r+=":"+$(this).nextAll(":not(.ayasel-before-selected):first").data("ayaidx");
		}
		ret+=r+" "
	});
	return ret.trim()
}