
/**
 * doAjaxUpdate does an ajax update.
 the $trigger element will be replaced by a progress icon
 the data will be submited in post body after replacing any
 occurence of [goalpk] / [day] by info inferred from tirgger.

 onSuccess callback, if set, is called with the response.
 */

let tpl_success= (res) => {
    if('new_award' in res) {
        if(res['new_award'])
            return $("#tpl-ajax-reward-success").html().replace('[new_award.icon]', res.new_award.icon);
        else
            return "+"+res.wonPoints;
    }
    return $("#tpl-ajax-success").html()
};
let tpl_error=() => {return $("#tpl-ajax-error").html()};
function doAjaxUpdate($trigger, data, onSuccess) {
    $trigger.html($("#tpl-ajax-updating").html());
    data = data
          .replace('[goalpk]',$trigger.parents('[data-goalpk]').data('goalpk'))
          .replace('[day]',$trigger.parents('[data-day]').data('day'));

    $.ajax({
        method:"POST",
        headers: {"X-CSRFToken":document.cookie.match(/csrftoken=([^ ]*)/)[1]},
        data:data,
        success: function(res){
            $trigger.replaceWith(tpl_success(res));
            if(onSuccess) onSuccess(res);
        },
        error: function(res){
            $trigger.replaceWith(tpl_error(res));
            location.reload();
        }
    })
}
window.addEventListener('load', ()=>{
    $("body").delegate(".gt-ajax-update","click",function(evt){
        evt.preventDefault();//Do not follow the link
        var $this = $(this);

        var data = $this.data("post");

        doAjaxUpdate($this, data, () => {
        });

    });
});