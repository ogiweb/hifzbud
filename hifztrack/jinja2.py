from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import reverse
from django.utils import translation

from jinja2 import Environment
from django.contrib.humanize.templatetags import humanize
from hifztrack import utils
from hifztrack.views.ui import ischeikh


def curr_challenge(user):
    from secret_word.models import SecretWordChallange
    return SecretWordChallange.objects.filter(user__pk=user.pk).first()


def environment(**options):
    env = Environment(extensions=['jinja2.ext.i18n'], **options)
    env.install_gettext_translations(translation)
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse,
        'naturaltime': humanize.naturaltime,
        'qrefs': utils.annotate_quranrefs,
        'ischeikh': ischeikh,
        'curr_challenge': curr_challenge
    })
    env.filters.update({
        'naturaltime': humanize.naturaltime,
        'naturalday': humanize.naturalday,
        'qrefs': utils.annotate_quranrefs,
        'qrefs2str': utils.quranrefs2str,
        'qrefs2html': utils.quranrefs2html
    })
    return env