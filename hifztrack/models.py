import os.path

from django.db import models
from django.db.models import Q, F
from django.contrib.auth.models import User
from django.utils.functional import cached_property
from django_gamification.models import GamificationInterface, PointChange
import datetime
from builtins import property
from django.db.models.aggregates import Sum, Max
from datetime import date, timedelta
from . import utils
import calendar
import qurandata
import secrets

EX_POINTS_PER_AYA=3


# Create your models here.
class Halka(models.Model):
    """ 
        A halka has a cheikh, some Talibs and some "Goal" 
    """
    cheikh = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+',help_text="The main cheikh of this halka", blank=True)
    associate_cheikhs= models.ManyToManyField(User, related_name='+', blank=True, help_text="Other users that have Cheikh role in this halka")
    name=models.CharField(max_length=200, default="")
    description=models.TextField(default="", blank=True)
    show_next_award=models.BooleanField(default=True, help_text="Show the next award to talib")
    read_access_token=models.CharField("Read access token", max_length=200, default=secrets.token_urlsafe)
    write_access_token=models.CharField("Write access token", max_length=200, default=secrets.token_urlsafe)
    miss_delay_days = models.PositiveIntegerField("Number of days after which goal is considered", default=31,
                                                  help_text="The maximum days after which plan is marked missed. In this case the behavious is"
                                                            " like it was not acheived.")

    def __str__(self):
        return "Halaka %s" % self.name
    
    def ischeikh(self, user):
        return (self.cheikh == user) or (self.associate_cheikhs.filter(pk=user.pk).exists())
    
    class Meta:
        ordering = ['name']

class AwardDefinition(models.Model):
    icon_html = models.TextField("HTML of award icon")
    name = models.CharField(max_length=200)
    unlock_challenge = models.BooleanField("Unlock a challenge when on", default=False)

    def __str__(self):
        return "Award: "+self.name
    
class HalkaAwardOnMonthlyProgressRule(models.Model):
    halka=models.ForeignKey(Halka,on_delete=models.CASCADE, related_name='month_award_rules')
    definition = models.ForeignKey(AwardDefinition, on_delete=models.CASCADE)
    unlock_percent = models.FloatField("Percentage of monthly target to give award.", default=100.0)
    
class AwardCounter(models.Model):
    """ Award counters can be linked to au user and award type,
    The counter tracks count of awards and is automatically incremented when this kind of awards is won.
    When marked featured=true it is shown in the top bar of the app.
    """
    user = models.ForeignKey(User, related_name='award_counters', on_delete=models.CASCADE)
    award = models.ForeignKey(AwardDefinition, related_name='+', on_delete=models.CASCADE)
    count = models.PositiveIntegerField("Number of available awards",default=0)
    featured = models.BooleanField("Show counter on site bar")   
    def __str__(self):
        return "'%s' of '%s' counter" % (self.award.name, self.user.get_full_name() or self.user.username)


def list_iconpacks():
    return [(x,x) for x in os.listdir(os.path.join(os.path.dirname(__file__), 'static/hifztrack/img/goal-iconpacks'))]


class Goal(models.Model):
    """
        Goal : some kind of thing we can acheive.
        => May be chall we decline this in sub types like revision, reading ...
        => KISS for the moment.
    """
    halka=models.ForeignKey(Halka,on_delete=models.CASCADE)
    name=models.CharField(max_length=200)
    description=models.TextField(default="")
    points_value=models.IntegerField(default=10, help_text="Represent the importance of goal. Default is 10")
    position=models.FloatField("Order in month overview", default=0, help_text="The lowest order is the first in the list. Notice that order can be negative : -1 is placed before 0, and that it is a float : 3.5 is placed between 3 and 4")
    start_day=models.DateField("Validity start", null=True, blank=True, help_text="This is inclusive : the goal is active on the start_day itself")
    end_day=models.DateField("Validity end", null=True, blank=True, help_text="This is inclusive : the goal is active on the end_day itself")

    icon_pack=models.CharField("Icon Pack", max_length=50, null=False, blank=False, default="chest", choices=list_iconpacks(), help_text="The icons to use at each step")

    goal_award=models.ForeignKey(AwardDefinition, null=True, blank=True, on_delete=models.SET_NULL);



    def isactive(self, day=None):
        if day == None: day = date.today()
        return (self.start_day == None or self.start_day <= day) and (self.end_day == None or self.end_day >= day)
    
    @classmethod
    def Q_active_at(cls, day1=None, daylast=None):
        """ Return a Q() object usable in filter to only active goals.
        IF day1 is given : returns qs filtered to goals active that day.
        If day1 and daylast are given : returns qs filterd to goals active at least one day 
          in that range.
        If none is set : returns goals active today.
        """
        
        if day1 == None: day1 = date.today()
        if daylast == None: daylast=day1
        return ((Q(start_day__isnull=True)|Q(start_day__lte=daylast)) &
            (Q(end_day__isnull=True)|Q(end_day__gte=day1)))
        
    @classmethod
    def qs_active_at(cls, day1=None, daylast=None):
        """ Return a queryset filter to only active goals.
        IF day1 is given : returns qs filtered to goals active that day.
        If day1 and daylast are given : returns qs filterd to goals active at least one day 
          in that range.
        If none is set : returns goals active today.
        """
        
        return Goal.objects.filter(Goal.Q_active_at(day1, daylast))
    
    @classmethod
    def qs_active_at_month(cls, day=None):
        """ Return a queryset filter to only goals active at least one day in 
        the provided day's month. If no day provided current month is considered"""
        if day == None: day = date.today()
        return Goal.qs_active_at(date(day.year,day.month,1),
                          utils.last_day_of_month(day.year, day.month))
        
    class Meta:
        ordering=["position","pk"]
        
    @cached_property
    def ayarefs(self):
        return list(qurandata.refs_from_text(self.name,ignoreinvalid=True))
    
    def __str__(self):
        return "Goal %s (for %s) " % (self.name, self.halka)

    class Meta:
        ordering = ('position',)
    
class Talib(models.Model):
    """
        Someone who attends a halka
    """
    halka = models.ForeignKey(Halka, on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='+', on_delete=models.CASCADE)
    game = models.ForeignKey(GamificationInterface, on_delete=models.CASCADE, null=True)
    last_dashboard_view_day=models.DateField(null=True,help_text="Last time dashboard was viewed to user successfullay and any progress / awards properly notified. NULL if never opened dashboard")
    show_dashboard_month_stats = models.BooleanField("Show statistics column in dashboard", default=True)
    points_month_tgt = models.IntegerField("Points per month",default=1000,help_text="Number of point that shall be acheived this month")
    last_tgt_upd_suggest = models.DateField("Date where target update was last suggested", default=date(2018,1,1))
    allow_self_review = models.BooleanField("Allow self review", default=False)
    enable_monthly_target_update = models.BooleanField("Automatically update target each month", default=True)
    
    
    def save(self, *args, **kwargs):
        """ Override to initialize game interfaces on creation of new instnce """
        if self.pk == None and self.game == None :
            self.game = GamificationInterface.objects.create()
        super(Talib, self).save(*args, **kwargs)
        
    def plan_goal(self, goal, day):
        return self.plannedgoal_set.create(goal=goal, day=day)
    
    def range_points_target(self, firstday, lastday):
        _,month_days = calendar.monthrange(firstday.year, firstday.month)
        return self.points_month_tgt * ((lastday-firstday).days + 1.0) / month_days
    
    def range_points_acquired(self, firstday, lastday):
        return (self.game.pointchange_set.filter(time__range=(firstday,lastday+timedelta(days=1)))
                                                 .aggregate(amount=Sum('amount')))['amount'] or 0
    
    def range_points_acheived_no_bonus(self, firstday, lastday):
        return (self.plannedgoal_set.filter(acheived=True, day__range=(firstday,lastday+timedelta(days=1)))
                                                 .aggregate(amount=Sum('goal__points_value')))['amount'] or 0
    
    
    def add_points(self, points, time=None):
        pc = self.game.pointchange_set.create(amount=points)
        #Using a separate update because of auto_now_add of pc.time
        if time != None: 
            pc.time = time
            pc.save()
        return pc
    
    def add_pending_points(self, goal, points, time=None, plannedgoal=None):
        PendingRewardPoints.objects.create(talib=self, goal=goal, 
                                    point_count=points, 
                                    date = time if time else datetime.datetime.now(),
                                    plannedgoal=plannedgoal)
    
    def reward_pending_points(self, goals, day=None):
        """ 
            Rewards pending points and grants related awards
        """
        today = date.today()
        (_,_,pct_before)= self.get_stats(today)
        
        qs = PendingRewardPoints.objects.filter(talib=self, goal__in=goals)
        if day :
            qs=qs.filter(date__date__lte=day)
            
        points = qs.aggregate(total=Sum('point_count'))['total']
        if points: 
            self.add_points(points)
        qs.delete()
        
        
        (_,_,pct_after)= self.get_stats(today)
        
        unlocked_awards = [r.definition for r in self.halka.month_award_rules.filter(unlock_percent__lte = pct_after,
                                                  unlock_percent__gt = pct_before).order_by('unlock_percent').all()]\
                        + [g.goal_award for g in goals if g.goal_award != None]
        tm = datetime.datetime.now()
        gcount=len(goals)
        for idx,a in enumerate(unlocked_awards):
            self.wonaward_set.create(win_time=tm, definition=a, win_goal=goals[idx%gcount])
            self.user.award_counters.filter(award=a).update(count=F('count')+1)
        return (unlocked_awards or [], points)
    
            
    @property
    def total_points(self):
        try:
            return self.game.points 
        except:
            return 0
        
    def get_full_name(self):
        return self.user.get_full_name() or self.user.username
    
    def get_stats(self, day = None):
        """ Returns tuple (points acquired at day, points target for month, percentage) """
        if day == None: day = date.today()
        day1 = date(day.year, day.month, 1)
        lastday = utils.last_day_of_month(day.year, day.month)

        points = self.range_points_acquired(day1, day)
        tgt = self.range_points_target(day1, lastday)
        pct = 0.0 if tgt == 0 else points * 100.0/tgt
        return (points, tgt, pct)
    
    def get_won_awards(self, day=None):
        if day == None: day = date.today()
        day1 = date(day.year, day.month, 1)
        ret = (self.wonaward_set.filter(win_time__date__range=(day1, day))
                .order_by('win_time').all())
        return ret

    def get_active_won_awards(self):
        # Select only WonAwards for which there is no planned goal 
        # in present or past after the win
        return WonAward.objects.filter(talib=self)\
                .annotate(last_pg_date=Max("talib__plannedgoal__day",
                            filter=Q(talib__plannedgoal__goal=F("win_goal"),
                                     talib__plannedgoal__day__lte=date.today())))\
                .filter(win_time__gte=F("last_pg_date"))
        
        
    def __str__(self):
        return "%s (at %s)" % (self.user,self.halka)

    class Meta:
        ordering=['pk']
        
class WonAward(models.Model):
    talib = models.ForeignKey(Talib, on_delete=models.CASCADE)
    definition = models.ForeignKey(AwardDefinition, on_delete=models.CASCADE)
    win_time = models.DateTimeField("Win Time")
    win_goal = models.ForeignKey(Goal, on_delete=models.SET_NULL, null=True)
    
    class Meta:
        ordering=["win_time"]
        
class PGStatus:
    """
    => missed (grey closed chest with chain) : planned but not acheived in the past
    => inprogress (colored closed chest) : planned for today
    => rewardable (colored jumping chest) : acheived not yet rewarded
    """
    MISSED="MISSED"
    INPROGRESS="INPROGRESS"
    ACHEIVED="ACHEIVED"
    
    
class PlannedGoal(models.Model):
    """
        The goal defines the intention of a talib to acheive a Goal in a given day.
    """
    talib = models.ForeignKey(Talib, on_delete=models.CASCADE, null=True)
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE)
    day = models.DateField(help_text="The day at which this goal is planned to be acheived.")
    acquired_points = models.ForeignKey(PointChange, help_text="PointChange triggered by completion of this planned goal of None if not yet completed", on_delete=models.CASCADE, null=True)
    quality_score = models.IntegerField("Quality acheival", default=0, help_text="Private score for sheich from 1 to 5, only for cheikh tracking")
    acheived = models.BooleanField("Goal acheived", default=False, help_text="Reward marked acheived (by cheikh).")
    
    class Meta:
        ordering = ["day","pk"]
    
    def mark_acheived(self, quality):
        self.quality_score = quality
        if not self.acheived:
            self.acheived = True
            self.talib.add_pending_points(self.goal, self.goal.points_value, 
                            datetime.datetime.fromordinal(self.day.toordinal()),self)
        self.save()
            
    @property
    def status(self):
        if self.acheived: return PGStatus.ACHEIVED
        if self.missed: return PGStatus.MISSED 
        return PGStatus.INPROGRESS

    @cached_property
    def miss_delay_days(self):
        return self.goal.halka.miss_delay_days
    @property
    def missed(self):
        if self.acheived: return False
        delta = date.today() - self.day 
        return delta.days >= self.miss_delay_days
        
    def __str__(self):
        return "Planned Goal for %s to acheive %s on %s : %s" % (self.talib,self.goal,self.day, "acheived" if self.acheived else "not acheived")

class PendingRewardPoints(models.Model):
    talib= models.ForeignKey(Talib, on_delete=models.CASCADE)
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE)
    date = models.DateTimeField()
    point_count = models.PositiveIntegerField("Number of points pending", default=0)
    plannedgoal = models.ForeignKey(PlannedGoal, 
                                    help_text="(Optional) Planned goal that caused these points (to cascade deletion mainly)",
                                    on_delete=models.CASCADE, null=True)

    
class GoalNote(models.Model):
    talib= models.ForeignKey(Talib, on_delete=models.CASCADE)
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE)
    author = models.ForeignKey(User, related_name='+', on_delete=models.CASCADE)
    create_date=models.DateTimeField(auto_now_add=True)
    content=models.TextField("Note for Talib", default="")
    private_to_cheikh=models.BooleanField("Show for cheikh only", default=False)
    
    ex_step_count=models.IntegerField(default=0,help_text="Number of steps of exercise: 0 means cannot exercise")
    ex_max_points=models.IntegerField(default=0,help_text="Number of points if exercise is completed 100%")
    
    ex_last_started=models.DateTimeField(null=True)
    ex_step = models.IntegerField("Current step the user is at", default=1)
    ex_skipped = models.IntegerField("Number of skipped ayas", default=0)
    ex_curr_score = models.IntegerField(default=0)
    
    ex_last_finished=models.DateTimeField(null=True)
    ex_last_final_score= models.IntegerField(null=True)
    
    @cached_property
    def ex_aya_refs(self):
        # We do not validate as we validate at reading time
        return qurandata.refs_from_text(self.content, context=self.goal.name)
    
    @cached_property
    def ex_max_score(self):
        return self.ex_step_count * 5
    
    @cached_property
    def ex_can_exercise(self):
        return self.ex_step_count > 0

    def ex_should_exercise(self, day=None):
        """ Should exercise if exercise is not older than 10 days """
        if day == None: day=date.today()
        return self.ex_can_exercise and (day - self.create_date.date()).days < 10 

    def ex_resume(self):
        """ Resume or start new exercice and return current step """
        if (self.ex_last_started == None 
            or self.ex_last_finished != None and (self.ex_last_finished > self.ex_last_started)
            or (date.today() - self.ex_last_started.date()).days > 1) :
            self.ex_step = 1
            self.ex_curr_score = 0
            self.ex_skipped = 0
            self.ex_last_started = datetime.datetime.now()
            self.save()
    
    def ex_end_step(self, grade):
        """ Return true to finish chain, false to continue """
        if grade :
            self.ex_curr_score += grade 
        else:
            self.ex_skipped += self.ex_aya_refs[self.ex_step-1].count()
            
        self.ex_step += 1
        
        if self.ex_step <= self.ex_step_count:
            self.save()
            return False
        
        #reward as we finished all the steps
        self.ex_last_finished = datetime.datetime.now()
        self.ex_last_final_score = self.ex_curr_score
        today = date.today()
        
        (_,_,pct_before)= self.talib.get_stats(today)
        
        points = -self.ex_skipped
        for ayaref in self.ex_aya_refs:
            points += ayaref.count()
        if points == 0: 
            return True
        
        points = points * EX_POINTS_PER_AYA
        
        self.talib.add_pending_points(self.goal, points, self.ex_last_finished)
        self.save() #FIXME: really ?
        return True
    
    def save(self, *args, **kwargs):
        """ validate refs on save """
        if self.pk == None: 
            ayarefs = qurandata.refs_from_text(self.content, context=self.goal.name, ignoreinvalid=False)
            self.ex_step_count = len(ayarefs)
            self.ex_max_points = 0
            for ref in ayarefs:
                self.ex_max_points += ref.count()
    
        super(GoalNote, self).save(*args, **kwargs)

class Sohba(models.Model):
    name=models.CharField(max_length=200, default="")
    talibs = models.ManyToManyField(Talib, help_text="Talibs participating to this sohba")
    
    def __str__(self):
        return "Sohba %s" % self.name
    
    class Meta:
        ordering = ['name']
    
def user_directory_path(rev, filename):
    return 'selfreview/user_{}/{}/{}/review_{}.ogg'.format(rev.talib.user.username, rev.day.year, rev.day.month ,rev.pk)

class SelfReview(models.Model):
    talib = models.ForeignKey(Talib, on_delete=models.CASCADE, help_text="Talib that made review")
    day = models.DateField(help_text="Date of self review")
    goals = models.ManyToManyField(Goal,blank=True, help_text="Goals reviewed")
    audio = models.FileField(upload_to=user_directory_path,help_text="Audio file of self recording")
    checkpoint_list = models.TextField("Checkpoint time list", null=True, help_text="Checkpoints during recording : Checkpoints time in seconds (integer) separated by commas")
    checked = models.BooleanField("Cheikh checked this review", default=False)
    checking_current_time = models.IntegerField("Time where cheikh stopped checking last", default=0)
    
    class Meta:
        ordering = ["day","pk"]
    
    def __str__(self):
        return "Review %s on %s" % (self.talib, self.day)
    
    @property
    def checkpoints(self):
        if self.checkpoint_list=='' or self.checkpoint_list == None:
            return []
        return (int(e) for e in self.checkpoint_list.split(','))
    
    @checkpoints.setter
    def checkpoints(self, l):
        self.checkpoint_list = ','.join(l)
        
        