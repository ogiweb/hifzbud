from rest_framework import serializers
from .models import SecretWordChallengeTemplate, Question

class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ['id', 'question', 'answer1', 'answer2', 'answer3', 'correct_answer']

class SecretWordChallengeTemplateSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True)

    class Meta:
        model = SecretWordChallengeTemplate
        fields = ['id', 'secret_word', 'questions']

    def create(self, validated_data):
        questions_data = validated_data.pop('questions')
        challenge_template = SecretWordChallengeTemplate.objects.create(**validated_data)
        for i, question_data in enumerate(questions_data):
            question_data['order'] = i
            question, _ = Question.objects.update_or_create(
                question=question_data['question'],
                defaults=question_data)
            challenge_template.questions.add(question)
        return challenge_template

    def update(self, instance, validated_data):
        questions_data = validated_data.pop('questions')
        instance.secret_word = validated_data.get('secret_word', instance.secret_word)
        instance.save()

        for question_data in questions_data:
            question, _ = Question.objects.update_or_create(
                question=question_data['question'],
                defaults=question_data)
            instance.questions.add(question)

        return instance
