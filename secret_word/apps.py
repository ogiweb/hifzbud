from django.apps import AppConfig


class SecretWordConfig(AppConfig):
    name = 'secret_word'
