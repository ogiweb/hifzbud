from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.utils.functional import cached_property
from django.views.generic import TemplateView

from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import SecretWordChallengeTemplate
from .serializers import SecretWordChallengeTemplateSerializer

from hifztrack.views.ui import POSTActionMixin
from secret_word.models import SecretWordChallange, Card


class SecretWordChallangeView(LoginRequiredMixin, POSTActionMixin, TemplateView):
    template_name = "challange/secret_word.html"

    @cached_property
    def challange(self):
        return get_object_or_404(SecretWordChallange, user=self.request.user)

    @cached_property
    def cards(self):
        return self.challange.card_set.order_by('disp_pos')

    def action_answer(self, cardpk, answernum):
        card=Card.objects.get(challenge=self.challange, pk=int(cardpk))
        return {"correct": card.do_answer(int(answernum))}

class CanChangeOrAddSWCT(permissions.BasePermission):
    """
    Custom permission to only allow users with specific permissions to edit or add Halka.
    """

    def has_permission(self, request, view):
        # Check for permission to add or change Halka
        if request.method in permissions.SAFE_METHODS:
            # Always allow GET, HEAD, OPTIONS requests.
            return True
        elif request.method == 'POST':
            return request.user.has_perm('secret_word.add_secretwordchallengetemplate')
        elif request.method in ['PUT', 'PATCH']:
            return request.user.has_perm('secret_word.change_secretwordchallengetemplate')
        else:
            return False

class SecretWordChallangeAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = SecretWordChallengeTemplateSerializer
    permission_classes = [permissions.IsAuthenticated, CanChangeOrAddSWCT]

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

class SecretWordChallangeListAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated, CanChangeOrAddSWCT]

    def get(self, request):
        challenges = SecretWordChallengeTemplate.objects.all()
        serializer = SecretWordChallengeTemplateSerializer(challenges, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = SecretWordChallengeTemplateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)