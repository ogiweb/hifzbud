from random import choice, shuffle

from django.contrib.auth.models import User
from django.db import models
from django.db.models import ManyToManyField, TextChoices, Manager
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _

# Create your models here.
from hifztrack.models import Talib, WonAward


class Question(models.Model):
    order = models.PositiveIntegerField(_("Administration order"), help_text="Shall be random")
    question = models.TextField(_("A question"))
    answer1 = models.TextField(_("Answer 1"))
    answer2 = models.TextField(_("Answer 2"))
    answer3 = models.TextField(_("Answer 3"))
    correct_answer=models.PositiveIntegerField(_("Right Answer"),
                                     choices=((0, "Answer 1"),(1, "Answer 2"),(2, "Answer 3")))


    @property
    def answers(self):
        return [self.answer1, self.answer2, self.answer3]

    def __str__(self):
        return f"Question: {self.question}"

    class Meta:
        ordering = ['order']


class SecretWordChallengeTemplate(models.Model):
    secret_word=models.CharField(_("The secret word"), max_length=20)
    questions=models.ManyToManyField(Question)

    def instanciate(self, swc):
        swc.succeeded=False
        swc.card_set.all().delete()
        cpos=list(range(0,len(self.secret_word)))
        shuffle(cpos)

        questions=list(self.questions.order_by('order'))
        for i,pos in enumerate(cpos):
            q=questions[pos%len(questions)]
            swc.card_set.create(question=q, letter=self.secret_word[pos],
                                disp_pos=i, status=CardStatus.HIDDEN)

    def __str__(self):
        return f"Challange: {self.secret_word}"

class SecretWordChallange(models.Model):
    user=models.OneToOneField(User, on_delete=models.CASCADE)
    template=models.ForeignKey(SecretWordChallengeTemplate, on_delete=models.CASCADE, null=True)
    succeeded=models.BooleanField(default=False)

    def actionnable(self):
        return self.card_set.filter(status=CardStatus.QUESTION).count()

    def reset(self):
        self.template.instanciate(self)

    def save(self, *args, **kwargs):
        do_init=self.pk == None
        ret = super().save(*args, **kwargs)
        if do_init: self.reset()
        return ret

    def unlock_random(self):
        card = self.card_set.filter(status=CardStatus.HIDDEN).order_by('question__order').first()
        if card is None: return
        card.status = CardStatus.QUESTION
        card.save()

    def __str__(self):
        return f"Challange of {self.user}"

@receiver(pre_save, sender=WonAward)
def unlock_on_win(instance, **kwargs):
    if instance.pk is not None: return
    if not instance.definition.unlock_challenge: return
    challenge = SecretWordChallange.objects.filter(user=instance.talib.user).first()
    if challenge is None: return
    challenge.unlock_random()



class CardStatus(TextChoices):
    HIDDEN = 'H',_("Hidden")
    QUESTION = 'Q', _("Queston")
    REVEALED = 'R', _("Revealed")


class Card(models.Model):
    challenge=models.ForeignKey("SecretWordChallange", on_delete=models.CASCADE)
    question=models.ForeignKey(Question, on_delete=models.CASCADE)
    disp_pos = models.PositiveIntegerField(_("Position to display card"), help_text="Shall be random")
    letter = models.CharField(_("Character"), max_length=3)
    status = models.CharField(_("Status"), max_length=1, choices=CardStatus.choices)

    @cached_property
    def can_try(self):
        return self.status == CardStatus.QUESTION

    def do_answer(self, num):
        if num == self.question.correct_answer:
            self.status = CardStatus.REVEALED
            self.save()
            return True
        else:
            return False
