from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from secret_word.models import Question, Card, SecretWordChallange, SecretWordChallengeTemplate


class CardAdminInline(admin.TabularInline):
    model=Card

class QuestionAdminInline(admin.TabularInline):
    model=Question


@admin.register(SecretWordChallengeTemplate)
class ChallengeTemplateAdmin(admin.ModelAdmin):
    model=SecretWordChallengeTemplate

@admin.register(SecretWordChallange)
class ChallengeAdmin(admin.ModelAdmin):
    model=SecretWordChallange
    inlines = (CardAdminInline,)
    actions = ('reset_challenge')
    def reset_challenge(self, queryset):
        for c in queryset:
            c.reset()


class QuestionResource(resources.ModelResource):
    class Meta:
        model = Question

@admin.register(Question)
class QuestionAdmin(ImportExportModelAdmin):
    model=Question
    resource_classes = [QuestionResource]