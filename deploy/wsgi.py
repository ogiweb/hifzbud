import os
import sys
from django.core.wsgi import get_wsgi_application
import hifzbud.settings

hifzbud.settings.DEBUG=False
hifzbud.settings.ALLOWED_HOSTS=['c3f092f21c954201b238a32ae304beac.testing-url.ws','hifzo.com']
hifzbud.settings.STATIC_URL="/static/"
hifzbud.settings.STATIC_ROOT = os.path.join(hifzbud.settings.BASE_DIR, "static")

hifzbud.settings.MEDIA_URL="/media/"
hifzbud.settings.MEDIA_ROOT = os.path.join(hifzbud.settings.BASE_DIR, "media")

#Working around gandi deployer ignoring editable requriements.txt in some way
editabledepsdir=os.path.abspath(os.path.join(os.path.dirname(__file__),'local','src'))

for file in os.listdir(editabledepsdir):
	sys.path.insert(0, os.path.join(editabledepsdir, file))

	
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hifzbud.settings")
from django.core.management import execute_from_command_line
execute_from_command_line(['manage.py','collectstatic','--no-input'])
execute_from_command_line(['manage.py','migrate'])


application = get_wsgi_application()

