from django.apps import AppConfig


class SimpletrackConfig(AppConfig):
    name = 'simpletrack'
