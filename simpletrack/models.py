from django.db import models
from hifztrack.models import Talib
from hifztrack import utils
import json

# Create your models here.

class ProgressRecord(models.Model):
    talib= models.ForeignKey(Talib, on_delete=models.CASCADE, null=True)
    target_day=models.DateField("Target Day", null=True, help_text="This is inclusive : the goal is active on the start_day itself")
    qadim_target=models.CharField("Quadim", max_length=200, default="")
    qadim_done=models.BooleanField("Quadim Finished", default=False)
    jadid_target=models.CharField("Jadid",max_length=200, default="")
    jadid_done=models.BooleanField("Jadid Finished", default=False)
    
    def decodeQadim(self):
        return utils.quranrefs2str(self.qadim_target)
    
    
    def decodeJadid(self):
        return utils.quranrefs2str(self.jadid_target)
    
class ProgressTalibOptions(models.Model):
    talib= models.OneToOneField(Talib, on_delete=models.CASCADE)
    format = models.TextField("Format JSON", default="{}")
    
    def get_format(self):
        try:
            return json.loads(self.format)
        except:
            return {}
    
    def set_format(self, formatjson, save=False):
        f=json.dumps(formatjson)
        if self.format == f: return
        self.format = f
        if save:
            self.save()
    
        