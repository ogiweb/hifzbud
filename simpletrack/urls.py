''' 
Created on 5 avr. 2018

@author: omar
'''
from django.urls.conf import path, include
from django.conf.urls import url
from datetime import date
from django.shortcuts import redirect
from django.http.response import HttpResponse
from simpletrack.views import HalkaProgressSheetView

urlpatterns =  [
                path('halkas/<int:pk>', HalkaProgressSheetView.as_view(),name='simple_sheet'),
               ]