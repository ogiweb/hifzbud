from django.shortcuts import render, get_object_or_404
from simpletrack.models import ProgressRecord, ProgressTalibOptions
from websheets.views import WebSheetView
from import_export.resources import ModelResource
from django.utils.functional import cached_property
from hifztrack.models import Halka, Talib
from django.contrib.auth.models import User
import import_export
from import_export.widgets import DateWidget, BooleanWidget
from datetime import datetime
import logging
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied

logger = logging.getLogger(__name__)


class OkWidget(BooleanWidget):
    """
    Widget for converting boolean fields.
    """
    def render(self, value, obj=None):
        return "Ok" if value else ""

    def clean(self, value, row=None, *args, **kwargs):
        return str(value).lower().strip() == "ok"


class ProgressRecordResourceBase(ModelResource):
    qadimok=import_export.fields.Field(attribute='qadim_done', column_name='qOk', widget=OkWidget())
    qadim=import_export.fields.Field(attribute='decodeQadim', readonly=True, column_name=' َAlt قديم')

    jadidok=import_export.fields.Field(attribute='jadid_done', column_name='jOk', widget=OkWidget())
    jadid=import_export.fields.Field(attribute='decodeJadid', readonly=True, column_name='Neu جديد')

    day=import_export.fields.Field(attribute='target_day', column_name='Tag', widget=DateWidget(format='%d/%m/%Y'))
  
    def __init__(self, talib):
        self.talib=talib
        
    def get_queryset(self):
        return ProgressRecord.objects.filter(talib=self.talib).order_by("target_day","id")
    
    def before_save_instance(self, instance, *args,**kwargs):
        instance.talib=self.talib
        super().before_save_instance(instance, *args,**kwargs)

class ProgressRecordResource(ProgressRecordResourceBase):
    class Meta:
        model=ProgressRecord
        exclude=('talib','qadim_done','target_day','jadid_done')

class ProgressRecordResourceRO(ProgressRecordResourceBase):
    class Meta:
        model=ProgressRecord
        exclude=ProgressRecordResource.Meta.exclude+('qadim_target','jadid_target')
      
class HalkaProgressSheetView(WebSheetView):
    """
        The cheikh and any talib can access the halka.
        The halka can also be accesses with ?SAT=<token> query param.
        if <token>=halka.read_access_token, access is read only, if <token>=halka.write_access_token access is read write.
        
    """
    
    
    hide_id_col=True
    delete_empty_rows=True
    
    
    
    def auth(self):
        if self.halka.ischeikh(self.request.user) or self.halka.talib_set.filter(user__username=self.request.user.username).first():
            return True
        token=self.request.GET.get('SAT','')
        if self.halka.read_access_token==token: return True
        if token and self.halka.write_access_token==token: return True
        return False
    
    def dispatch(self, request, *args, **kwargs):
        if not self.auth(): raise PermissionDenied
        return WebSheetView.dispatch(self, request, *args, **kwargs)
    
    @cached_property
    def halka(self):
        return get_object_or_404(Halka, pk=self.kwargs['pk'])
    
    def get_allow_save(self):
        if self.halka.ischeikh(self.request.user): return True
        token=self.request.GET.get('SAT','')
        if token and self.halka.write_access_token==token: return True
        return False
    
    def get_sheet_resource_classes(self):
        res_class= ProgressRecordResource if self.get_allow_save() else ProgressRecordResourceRO
        return [ (talib.get_full_name(),res_class(talib)) 
            for talib in self.halka.talib_set.all()]
        
    def get_resource_for_new_sheet(self, sheetname):
        talib=self.halka.talib_set.filter(user__first_name=sheetname).first()
        if talib == None:
            idx=0
            pfx="__user%s"%datetime.strftime(datetime.now(),"%Y%m%d")
            user=True
            while user:
                idx=idx+1
                user = User.objects.filter(username=pfx+'_%d__'%idx).first()
            user = User.objects.create_user(pfx+'_%d__'%idx, '', '')
            user.first_name=sheetname
            user.save()
                
            talib = Talib.objects.create(halka=self.halka, user=user)
        return ProgressRecordResource(talib)
    
    def handle_missing_sheet_onsave(self, sheetname, resource):
        talib=self.halka.talib_set.filter(user__first_name=sheetname).first()
        if talib: talib.delete() #Delete talib from halka
        
    def get_sheet_format(self, sheetname):
        options=ProgressTalibOptions.objects.filter(talib__halka=self.halka, 
                                    talib__user__first_name=sheetname).first()
        return options.get_format() if options else {}
    
    def update_sheet_format(self, sheetname, formatjson):
        talib=self.halka.talib_set.filter(user__first_name=sheetname).first()
        if talib:
            options, _created=ProgressTalibOptions.objects.get_or_create(talib=talib)
            options.set_format(formatjson, save=True)
        else:
            logger.warn("Unknown talib for sheet %s, ignoring save formatting" % sheetname)