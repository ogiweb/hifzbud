from types import SimpleNamespace

import paramiko
import time
import sys
import click
import os

LOGS = {
    'error': '/lamp0/var/log/www/uwsgi.log',
    'access': '/lamp0/var/log/apache/access.log'
}


SERVERS = {
    'prod': {
        'logs': SimpleNamespace(user='374547', host='sftp.sd6.gpaas.net', port=22),
        'ssh': SimpleNamespace(user='374547', host='console.sd6.gpaas.net', port=22, init_cmd="bash -c 'cd /lamp0/web/vhosts/default; DJANGO_SETTINGS_MODULE=appsettings python manage.py shell'"),
    }
}


@click.group()
@click.option('--server', default=None, help=f"Can be one of :{' '.join(SERVERS.keys())}")
def ttcli(server):
    global config, env
    env = server or next(iter(SERVERS.keys()))
    click.echo(f'--server not specified, defaulting to {env}')
    config = SERVERS[env]


@ttcli.command()
@click.option('-h', default=1000, help="Number of bytes to list")
@click.option('-f', is_flag=True, default=False, help="Follow the logs")
@click.argument('log', default='error')  # , help="Either error or access")
def tail(log, h, f):
    c=config['logs']
    click.echo(f'Connecting to {env}: {c.host}:{c.port}')
    log = LOGS[log]
    with paramiko.SSHClient() as cli:
        cli.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        passwd = os.getenv('TTPASS')
        if not passwd:
            print('TTPASS environement variable not set, please type in the password')
            passwd = input('password:')

        cli.connect(hostname=c.host, port=c.port, username=c.user, password=passwd)
        with cli.open_sftp() as sftp:  # is this really needed if we close underlining transport ?
            with sftp.open(log, 'r') as fin:
                s = fin.stat()
                fin.seek(s.st_size - h)
                while True:
                    r = fin.read()
                    if r:
                        print(r.decode('utf-8'))
                    if not f: return
                    time.sleep(5)


def windows_shell(chan, init_cmd=None):
    import threading

    sys.stdout.write(
        "Line-buffered terminal emulation. Press F6 or ^Z to send EOF.\r\n\r\n"
    )

    def writeall(sock):
        while True:
            data = sock.recv(256)
            if not data:
                sys.stdout.write("\r\nTerminated, press enter to quit.\r\n\r\n")
                sys.stdout.flush()
                break
            sys.stdout.write(data.decode('utf-8'))
            sys.stdout.flush()

    writer = threading.Thread(target=writeall, args=(chan,))
    writer.start()


    try:
        if init_cmd:
            chan.send(init_cmd.strip()+'\n')
        while True:
            try:
                d = sys.stdin.read(1)
                if not d:
                    break
                chan.send(d)
            except KeyboardInterrupt:
                chan.send(b'\x03') #Send Ctrl+C
    except OSError as e:
        pass
    except EOFError:
        # user hit ^Z or F6
        pass

@ttcli.command()
@click.option('--cmd', default='-', help="Command to run, defaults to python shell")
def shell(cmd):
    c=config['ssh']
    click.echo(f'Connecting to {env}: {c.host}:{c.port}')
    with paramiko.SSHClient() as cli:
        cli.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        print(f"Connecting {c.user}@{c.host}")
        cli.connect(hostname=c.host, port=c.port, username=c.user)
        windows_shell(cli.invoke_shell(), c.init_cmd if cmd == '-' else cmd)


ttcli()
